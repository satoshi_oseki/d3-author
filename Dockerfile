FROM microsoft/dotnet:2.1.301-sdk AS builder
WORKDIR /source

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY package*.json ./
RUN npm install
COPY ./ ./
RUN npm run build

COPY *.csproj .
RUN dotnet restore

COPY ./ ./

RUN dotnet publish "./d3-author.csproj" --output "./dist" --configuration Release --no-restore

FROM microsoft/dotnet:2.1.5-aspnetcore-runtime
WORKDIR /app
COPY --from=builder /source/dist .

ENV ASPNETCORE_ENVIRONMENT Production
ENV ASPNETCORE_URLS http://+:8080
EXPOSE 8080
ENTRYPOINT ["dotnet", "d3-author.dll"]
