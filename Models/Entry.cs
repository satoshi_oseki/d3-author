namespace d3_author.Models
{
    public abstract class Entry
    {
        public string Id { get; set; } // GUID
        public STATUS Status { get; set; }
    }
}