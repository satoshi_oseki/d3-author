namespace d3_author.Models
{
    public class Note
    {
        public string ID { get; set; } // uuid
        public string Text { get; set; }
        public long? Updated { get; set; } // UNIX epoch time
        public bool HasBeenRead { get; set; }
    }
}
