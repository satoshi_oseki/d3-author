using System.Collections;
using System.Collections.Generic;

namespace d3_author.Models
{
    public class DataRow
    {
        private List<string> data;
        public List<string> ItemArray { get { return data; } }

        public DataRow()
        {
            data = new List<string>();
        }
        
        public void Add(string value) {
            if (data == null)
            {
                data = new List<string>();
            }
            
            data.Add(value); }
    }

    public class DataTable
    {
        private List<DataRow> data;
        public IEnumerable Rows { get { return data; } }

        public DataTable(int numColumns, int numRows)
        {
            if (data == null) {
                data = new List<DataRow>();
            }

            for (var row = 0; row < numRows; row++)
            {
                data.Add(new DataRow());
                for (var col = 0; col < numColumns; col++)
                {
                    data[row].Add("");
                }
            }
        }

        public string GetData(int col, int row)
        {
            return data[row].ItemArray[col];
        }

        public void SetData(int col, int row, string value)
        {
            data[row].ItemArray[col] = value;
        }
    }
}