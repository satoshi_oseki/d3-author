namespace d3_author.Models
{
    public class Staff
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool IsAdmin { get; set; }
        public int Order { get; set; }
    }
}