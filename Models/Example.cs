namespace d3_author.Models
{
    public class Example : TextOption
    {
      public int LocalId { get; set; } // 0 to 3
      public LANG Language { get; set; }

      public Example Clone() {
        return (Example) this.MemberwiseClone();
      }
    }
}