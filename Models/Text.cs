namespace d3_author.Models
{
    public abstract class Text : Entry
    {
        public string Value { get; set; }
        public string AuthorId { get; set; } // GUID
        public string Author { get; set; } // username
        public long Created { get; set; } // Unix Epoc
        public long Updated { get; set; } // Unix Epoc
        public STAGE Stage { get; set; }
    }
}