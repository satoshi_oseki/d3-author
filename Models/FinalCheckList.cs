namespace d3_author.Models
{

    public class FinalCheckList
    {
        public string Id { get; set; }
        public string VerbId { get; set; }
        public bool Kanji { get; set; }
        public bool EnglishIndex { get; set; }
        public bool Link1 { get; set; } // 自他対応
        public bool Link2 { get; set; } // 同義異格配列
    }
}
