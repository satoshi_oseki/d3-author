using System.Collections.Generic;

namespace d3_author.Models
{
    public class VerbListResponse
    {
        public List<Verb> Verbs { get; set; }
        public long NumVerbs { get; set; }

        public VerbListResponse()
        {
        Verbs = new List<Verb>();
        }
    }
}
