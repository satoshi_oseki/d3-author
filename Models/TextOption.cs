namespace d3_author.Models
{
    public abstract class TextOption : Text
    {
        public string VerbId { get; set; }
        public bool Selected { get; set; } // Best choice
    }
}