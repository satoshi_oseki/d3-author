using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MongoDB.Bson.Serialization.Attributes;

namespace d3_author.Models
{
    public class Verb : Text
    {
        public int LocalIndex { get; set; } // index within a verb
        public string GroupId { get; set; } // GUID
        public string Structure { get; set; }
        public string EnglishIndex { get; set; }
        //public List<string> FullSentenceDefinition { get; set; }
        public string FullSentenceDefinition { get; set; }
        public string NoteForReferenceBook { get; set; }
        public string RepresentativeExample { get; set; }
        public string NoteForExampleAuthor { get; set; }
        public string GeneralNotes { get; set; }
        public string JpAssigneeId { get; set; } // GUID
        public string JpCheckerId { get; set; } // GUID
        public string EnAssigneeId { get; set; } // GUID
        public string EnCheckerId { get; set; } // GUID
        public string RefAssigneeId { get; set; } // GUID
        public string TurnId { get; set; } // GUID
        public List<Message> Messages { get; set; }
        //public virtual List<Example> JpExamples { get; set; }
        //public virtual List<Example> EnExamples { get; set; }
        //public List<Tuple<Example, Example>>[] Examples { get; set; }
        [BsonIgnore]
        public bool Selected { get; set; }
        public List<User> Assignees { get; set; }
        [NotMapped]
        public IEnumerable<string> Links1 { get; set; } // 自他対応
        public string Links1String
        {
            get { return Links1 != null ? string.Join(",", Links1) : null; }
            set { Links1 = string.IsNullOrEmpty(value) ? new string[0] : value.Split(','); }
        }
        [NotMapped]
        public IEnumerable<string> Links2 { get; set; } // 同義異格配列
        public string Links2String
        {
            get { return Links2 != null ? string.Join(",", Links2) : null; }
            set { Links2 = string.IsNullOrEmpty(value) ? new string[0] : value.Split(','); }
        }

        [NotMapped]
        public string IdForLinks { get; set; }
        [NotMapped]
        public string Links1Raw { get; set; }
        [NotMapped]
        public string Links2Raw { get; set; }

        public Verb()
        {
            JpAssigneeId = "00000000-0000-0000-0000-000000000000";
            EnAssigneeId = "00000000-0000-0000-0000-000000000000";
            //Examples = new List<Tuple<Example, Example>>[(int)STAGE.STAGE_8];
        }
    }
}