namespace d3_author.Models
{
    // FSD
    // English translations 0 - 3
    // Notes for reference book
    public class Suggestion
    {
        public string Id { get; set; }
        public string VerbId { get; set; }
        public string FullSentenceDefinition { get; set; }
        public string[] Translations { get; set; } // 0 - 3
        public string NoteForReferenceBook { get; set; }
    }
}
