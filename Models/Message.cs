namespace d3_author.Models
{
    public class Message : Entry
    {
        public string VerbId { get; set; }
        public string User { get; set; } // username
        public string Content { get; set; }
        public long? Created { get; set; } // Unix Epoc
        public long? Updated { get; set; } // Unix Epoc

        public string ReadBy { get; set; } // a list of usernames separated by a comma
    }
}
