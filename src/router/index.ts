import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import MainView from '../components/MainView.vue'
import store from '../store'
import UploadExcelFile from '../components/UploadExcelFile.vue';

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'MainView',
            component: MainView,
            meta: { requiresAuth: true }
        }, {
            path: '/Login',
            name: 'Login',
            component: Login
        }, {
            path: '/UploadExcelFile',
            name: 'UploadExcelFile',
            component: UploadExcelFile
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !store.state.authUser) {
        next({ path: '/Login', query: { redirect: to.fullPath } });
    } else {
        next();
    }
});

export default router;