import Vue from 'vue'
import { Filter } from 'vue-ts-decorate';

export default Vue.filter('wrap', function (value: string, begin: string, end: string): string {
    return value ? begin + value + end : '';
});
