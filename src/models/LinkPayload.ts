import { Verb } from './Verb';

export interface LinkPayload {
    verb: Verb;
    linked: Verb;
    group: number; // 1 | 2
}