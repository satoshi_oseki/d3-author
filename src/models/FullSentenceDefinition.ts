import { TextEntry } from "./Text";

export interface FullSentenceDefinition extends TextEntry {
    verbId: string;
}