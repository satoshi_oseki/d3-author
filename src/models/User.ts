export interface User {
    displayName: string;
    email: string;
    firstName: string;
    lastName: string;
    roles: string[];
    userId: string;
    username: string;
}
