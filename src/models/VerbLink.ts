import { Verb } from './Verb';

export interface VerbLink {
    id: string;
    value: string;
    structure: string;
}