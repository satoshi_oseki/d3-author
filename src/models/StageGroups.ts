import { User } from "./User";

export interface StageGroups {
    stage5group: User;
    stage8group: User;
    stage9group: User;
}
