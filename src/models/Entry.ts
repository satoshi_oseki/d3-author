export interface Entry {
    id: string;
    status: number; // active: 0, deleted: 1
}
