export interface Note {
    id: string;
    text: string;
    updated: number;
    hasBeenRead: boolean;
}
