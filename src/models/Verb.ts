import { Message } from './Message';
import { Note } from './Note';
import { TextEntry } from './Text';

export interface Verb extends TextEntry {
    localIndex: number;
    groupId: string;
    structure: string;
    englishIndex: string;
    fullSentenceDefinition: string;
    noteForReferenceBook: string;
    representativeExample: string;
    noteForExampleAuthor: string;
    generalNotes: string;
    jpAssigneeId: string;
    enAssigneeId: string;
    refAssigneeId: string;
    jpCheckerId: string;
    enCheckerId: string;
    refCheckerId: string;
    assignees: string[];
    turnId: string;
    messages: Message[];
    selected: boolean;
    links1String: string;
    links1: string[]; // Verb id list
    links2String: string;
    links2: string[]; // Verb id list
}
