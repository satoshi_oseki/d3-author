import { TextEntry } from "./Text";

export interface Example extends TextEntry {
    localId: number;
    list: any[];
}