export interface FinalCheckList {
    kanji: boolean;
    englishIndex: boolean;
    link1: boolean;
    link2: boolean;
}
