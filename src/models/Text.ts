import { Entry } from './Entry';

export interface TextEntry extends Entry {
    value: string;
    authorId: string;
    author: string;
    created: number;
    updated: number;
    stage: number;
}
