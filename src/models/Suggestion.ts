export interface Suggestion {
    id: string;
    fullSentenceDefinition: string;
    translations: string[];
    noteForReferenceBook: string;
}
