﻿import { FullSentenceDefinition } from './FullSentenceDefinition';
import { FinalCheckList } from './FinalCheckList';
import { Suggestion } from './Suggestion';
import { Candidates } from './Candidates';
import { Example } from './Example';
import { User } from './User';
import { Verb } from './Verb';
import { StageGroups } from './StageGroups';

export interface D3State {
    loaded: { verbs: boolean },
    tokenStorageKey: string,
    userStorageKey: string,
    authUser: string,
    currentUser: any, //object,
    verbs: Verb[],
    activeVerb: Verb,
    jpExamples: Example[][], // index in page, 0 - 3
    enExamples: Example[][], // index in page, 0 - 3
    comments: Object,
    candidates: Candidates, // { list, selectedIndex } used for fullSentenceDefinitions and notes for reference book
    latestFullSentenceDefinition: FullSentenceDefinition,
    finalFullSentenceDefinition: FullSentenceDefinition,
    users: User[],
    jpMembers: User[],
    enMembers: User[],
    jpManagers: User[],
    enManagers: User[],
    managers: User[],
    stageGroups: StageGroups,
    mode: DisplayMode,
    filterStage: number,
    filterTurn: string,
    page: number,
    numVerbs: number,
    numItemsPerPage: number,
    canGoToPrevPage: boolean,
    canGoToNextPage: boolean,
    searchWord: string,
    showSaveCompleteAlert: boolean,
    currentStage: number, // 1-based
    canChangeStage: boolean
    suggestions: Suggestion, // Summary uses it in stage 8
    finalCheckList: FinalCheckList
}

export type DisplayMode = 'normal' | 'stage2' | 'stage6'

