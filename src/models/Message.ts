import { Entry } from './Entry';

export interface Message {
    verbId: string;
    user: string;
    created: number;
    updated?: number;
    content: string;
    readBy: string; // a list of usernames separated by a comma
}
