import { FinalCheckList } from './models/FinalCheckList';
import { Suggestion } from './models/Suggestion';
import { LinkPayload } from './models/LinkPayload';
import Vue from 'vue'
import Vuex from 'vuex'

import { Message } from './models/Message';
import { Verb } from './models/Verb';

import axios, { AxiosResponse } from 'axios'
import * as _ from 'lodash'

import { D3State } from './models/D3State';
import { Example } from './models/Example';
import { User } from './models/User';
import { StageGroups } from './models/StageGroups';
//import VueResource from 'vue-resource'
//import Verbs from './mockData'

Vue.use(Vuex)
//Vue.use(VueResource)
//import Multiselect  from 'vue-multiselect'

//Vue.component(Multiselect)
// Polyfill for window.fetch()
// require('whatwg-fetch')

const store = new Vuex.Store<D3State>({
    state: {
        loaded: { verbs: false },
        tokenStorageKey: 'd3',
        userStorageKey: 'd3-user',
        authUser: localStorage.getItem('d3'),
        currentUser: JSON.parse(localStorage.getItem('d3-user')),
        verbs: [],
        activeVerb: null,
        users: [],
        jpExamples: [], // 0 - 3
        enExamples: [], // 0 - 3
        comments: {},
        candidates: { list: [], selectedIndex: -1 }, // { list, selectedIndex } used for fullSentenceDefinitions and notes for reference book
        latestFullSentenceDefinition: null,
        finalFullSentenceDefinition: null,
        jpMembers: [],
        enMembers: [],
        jpManagers: [],
        enManagers: [],
        managers: [],
        stageGroups: {} as StageGroups,
        mode: 'normal',
        filterStage: null,
        filterTurn: '',
        page: 0,
        numVerbs: 0,
        numItemsPerPage: 20,
        canGoToPrevPage: true,
        canGoToNextPage: true,
        searchWord: '',
        showSaveCompleteAlert: false,
        currentStage: 0,
        canChangeStage: true,
        suggestions: {
            fullSentenceDefinition: '',
            translations: ['', '', '', ''],
            noteForReferenceBook: ''
        } as Suggestion,
        finalCheckList: {
            kanji: false,
            englishIndex: false,
            link1: false,
            link2: false
        }
    },
    getters: {
        canSave: state => {
            return state.currentStage < 10
                || state.finalCheckList.englishIndex
                && state.finalCheckList.kanji
                && state.finalCheckList.link1
                && state.finalCheckList.link2;
        }
    },
    // mutations: {
    //     ADD_ENTRY: (state, verb): void => {
    //         state.verbs.splice(0, 0, verb);
    //     },
    //     SET_USER: (state, user): void => {
    //         if (user && user.token) {
    //             Vue.set(state, 'authUser', user.token);
    //             Vue.set(state, 'currentUser', user);
    //             localStorage.setItem(state.tokenStorageKey, user.token);
    //             localStorage.setItem(state.userStorageKey, JSON.stringify(user));
    //         } else {
    //             state.authUser = null;
    //             localStorage.removeItem(state.tokenStorageKey);
    //             localStorage.removeItem(state.userStorageKey);
    //         }
    //     },
    //     SET_JP_MEMBERS: function (state, members) {
    //         Vue.set(state, 'jpMembers', members);
    //     },
    //     SET_EN_MEMBERS: function (state, members) {
    //         Vue.set(state, 'enMembers', members);
    //     },
    //     SET_VERBS: (state, verbListResponse) => {
    //         Vue.set(state, 'numVerbs', verbListResponse.numVerbs);
    //         Vue.set(state, 'verbs', verbListResponse.verbs);
    //         Vue.set(state, 'canGoToPrevPage', state.page > 0);
    //         Vue.set(state, 'canGoToNextPage', state.page < Math.ceil(state.numVerbs / state.numItemsPerPage) - 1);
    //         console.log('canGoToPrevPage ' + state.canGoToPrevPage)
    //         console.log('canGoToNextPage ' + state.canGoToNextPage)
    //     },
    //     SET_CAN_CHANGE_STAGE: function (state) {
    //         Vue.set(state, 'canChangeStage', true);
    //     },
    //     SET_ACTIVE_VERB: function (state, verb) {
    //         if (state.activeVerb) {
    //             Vue.set(state.activeVerb, 'selected', false);
    //         }
    //         if (state.activeVerb !== verb) {
    //             verb.selected = !verb.selected;
    //             Vue.set(state, 'activeVerb', verb);
    //         }
    //     },
    // },
    mutations: {
        ADD_ENTRY: function (state, verb) {
            state.verbs.splice(0, 0, verb);
        },
        SEARCH: function (state, word) {
            Vue.set(state, 'searchWord', word);
        },
        CHANGE_MODE: function (state, mode) {
            Vue.set(state, 'mode', mode);
        },
        FILTER_BY_STAGE: function (state, stage) {
            Vue.set(state, 'filterStage', stage);
        },
        FILTER_BY_TURN: function (state, turn) {
            Vue.set(state, 'filterTurn', turn);
        },
        GO_TO_PREV_PAGE: function (state) {
            if (state.page > 0) {
                Vue.set(state, 'page', state.page - 1);
            }
        },
        GO_TO_NEXT_PAGE: function (state) {
            if (state.page < Math.ceil(state.numVerbs / state.numItemsPerPage) - 1) {
                Vue.set(state, 'page', state.page + 1);
            }
        },
        CHANGE_PAGE: function (state, page) {
            Vue.set(state, 'page', page);
        },
        SET_ACTIVE_VERB: function (state: D3State, verb: Verb) {
            Vue.set(state, 'activeVerb', verb);
        },
        UPDATE_ENTRY: function (state, verb) {
            let index = _.findIndex(state.verbs, (v) => verb.id === v.id);
            verb.selected = true; // to keep dialog open. it's false in response because it isn't saved on the server side
            state.verbs.splice(index, 1, Object.assign({}, verb)); // replace old one with new one
        },
        DELETE_ENTRY: function (state, data /* { verb, index } */) {
            state.verbs.splice(data.index, 1);
        },
        CLONE_ENTRY: function (state, data /* { verb, index } */) {
            state.verbs.splice(data.index, 0, data.verb);
        },
        SET_USER: function (state, user) {
            if (user && user.token) {
                Vue.set(state, 'authUser', user.token);
                Vue.set(state, 'currentUser', user);
                localStorage.setItem(state.tokenStorageKey, user.token);
                localStorage.setItem(state.userStorageKey, JSON.stringify(user));
            } else {
                state.authUser = null;
                localStorage.removeItem(state.tokenStorageKey);
                localStorage.removeItem(state.userStorageKey);
            }
        },
        SET_USERS: (state, users) => {
            Vue.set(state, 'users', users);
        },
        SET_JP_MEMBERS: function (state, members) {
            Vue.set(state, 'jpMembers', members);
        },
        SET_EN_MEMBERS: function (state, members) {
            Vue.set(state, 'enMembers', members);
        },
        SET_JP_MANAGERS: function (state, managers) {
            Vue.set(state, 'jpManagers', managers);
        },
        SET_EN_MANAGERS: function (state, managers) {
            Vue.set(state, 'enManagers', managers);
        },
        SET_MANAGERS: function (state: D3State, managers: User[]) {
            Vue.set(state, 'managers', managers)
        },
        SET_STAGE_GROUPS: function (state: D3State, groups: User[]) { // username: stage5group, stage8group, stage9group for turns
            let stageGroups = {} as StageGroups;
            groups.map(g => {
                stageGroups[g.username] = g;
            });
            Vue.set(state, 'stageGroups', stageGroups);
        },
        SET_CURRENT_STAGE: (state, value: number) => {
            Vue.set(state, 'currentStage', value);
        },
        SET_VERBS: function (state, verbListResponse) {
            Vue.set(state, 'numVerbs', verbListResponse.numVerbs);
            Vue.set(state, 'verbs', verbListResponse.verbs);
            Vue.set(state, 'canGoToPrevPage', state.page > 0);
            Vue.set(state, 'canGoToNextPage', state.page < Math.ceil(state.numVerbs / state.numItemsPerPage) - 1);
            console.log('canGoToPrevPage ' + state.canGoToPrevPage)
            console.log('canGoToNextPage ' + state.canGoToNextPage)
        },
        SET_JP_EXAMPLES: function (state, payload) {
            if (!state.jpExamples[payload.index]) {
                Vue.set(state.jpExamples, payload.index, []);
            }
            Vue.set(state.jpExamples[payload.index], payload.localId, payload.data);
            console.log('********* ' + JSON.stringify(state.jpExamples));
        },
        SET_EN_EXAMPLES: function (state, payload) {
            if (!state.enExamples[payload.index]) {
                Vue.set(state.enExamples, payload.index, []);
            }
            Vue.set(state.enExamples[payload.index], payload.localId, payload.data);
        },
        SET_FULL_SENTENCE_DEFINITIONS: function (state, payload) {
            console.log('SET_FULL_SENTENCE_DEFINITIONS:' + JSON.stringify(payload))
            Vue.set(state, 'candidates', payload);
            console.log('SET_FULL_SENTENCE_DEFINITIONS 2:' + JSON.stringify(state.candidates))
        },
        SET_REFERENCE_BOOK_NOTES: function (state, payload) {
            console.log('SET_REFERENCE_BOOK_NOTES:' + JSON.stringify(payload))
            Vue.set(state, 'candidates', payload);
            console.log('SET_REFERENCE_BOOK_NOTES 2:' + JSON.stringify(state.candidates))
        },
        SET_COMMENTS: function (state, payload) {
            Vue.set(state.comments, payload.linkId, payload.comments);
        },
        ADD_EXAMPLE: function (state, payload /* { index, lang, verbId, stage, localId } */) {
            const index = payload.index;
            let key = payload.localId;
            const lang = payload.lang;
            if (lang === 1 && !state.jpExamples[index]) {
                Vue.set(state.jpExamples, index, []);
            }
            if (lang === 2 && !state.enExamples[index]) {
                Vue.set(state.enExamples, index, []);
            }
            let examples = payload.lang === 1 ? state.jpExamples[index] : state.enExamples[index];
            if (!examples[key]) {
                examples[key] = { list: [] } as Example;
            }
            let exampleList = examples[key].list;
            exampleList.push({ value: '', language: payload.lang, verbId: payload.verbId, stage: payload.stage, localId: payload.localId, author: state.currentUser.username, status: 0, selected: false })
        },
        UPDATE_EXAMPLE: function (state, data /* { index, example, localId } */) {
            let key = data.example.localId;
            const index = data.index;
            let examples = data.example.language === 1 ? state.jpExamples[index][key].list : state.enExamples[index][key].list;
            Vue.set(examples, data.localId, data.example);
            Vue.set(state, 'showSaveCompleteAlert', true);
            setTimeout(function () { Vue.set(state, 'showSaveCompleteAlert', false); }, 3000);
        },
        UPDATE_CAN_CHANGE_STAGE: function (state, language) {
            let examples = language === 1 ? state.jpExamples : state.enExamples;
            let result = true;
            _.forEach(examples, function (value) {
                if (_.every(value.list, { 'selected': false })) {
                    result = false;
                }
            })
            Vue.set(state, 'canChangeStage', result);
        },
        SET_CAN_CHANGE_STAGE: function (state) {
            Vue.set(state, 'canChangeStage', true);
        },
        RESET_CAN_CHANGE_STAGE: function (state) {
            Vue.set(state, 'canChangeStage', false);
        },
        DELETE_EXAMPLE: function (state, data /* { index, example, localId } */) {
            let key = data.example.localId;
            let examples = data.example.language === 1
                ? state.jpExamples[data.index][key]
                : state.enExamples[data.index][key];
            let exampleList = examples.list;
            if (data.example.selected) {
                Vue.set(examples, 'selectedIndex', -1); // none selected
                Vue.set(state, 'canChangeStage', false);
            }
            exampleList.splice(data.index, 1);
            Vue.set(examples, 'list', exampleList);
        },
        ADD_COMMENT: function (state, linkId) {
            let copy = Object.assign([], state.comments[linkId]);
            if (!copy) {
                copy = [];
            }
            copy.push({ value: '', linkId: linkId, author: state.currentUser.username, status: 0 });
            Vue.set(state.comments, linkId, copy);
        },
        UPDATE_COMMENT: function (state, data /* { comment, index } */) {
            Vue.set(state.comments[data.comment.linkId], data.index, data.comment);
            Vue.set(state, 'showSaveCompleteAlert', true);
            setTimeout(function () { Vue.set(state, 'showSaveCompleteAlert', false); }, 3000);
        },
        DELETE_COMMENT: function (state, data) {
            state.comments[data.comment.linkId].splice(data.index, 1);
        },
        ADD_CANDIDATE: function (state, payload /* { verbId, stage } */) {
            let candidates = state.candidates;
            if (candidates && candidates.list && candidates.list.length === 0) {
                candidates.selectedIndex = -1;
            }
            let candidateList = candidates.list;

            candidateList.push({ value: '', verbId: payload.verbId, stage: payload.stage, author: state.currentUser.username, status: 0, selected: false })
        },
        UPDATE_CANDIDATE: function (state, data /* { candidate, index } */) {
            let candidates = state.candidates.list;
            Vue.set(candidates, data.index, data.candidate);
            Vue.set(state, 'showSaveCompleteAlert', true);
            setTimeout(function () { Vue.set(state, 'showSaveCompleteAlert', false); }, 3000);
        },
        DELETE_CANDIDATE: function (state, data /* { candidate, index } */) {
            let list = state.candidates.list;
            if (data.candidate.selected) {
                Vue.set(state.candidates, 'selectedIndex', -1); // none selected
            }
            list.splice(data.index, 1);
            Vue.set(state.candidates, 'list', list);
        },
        SET_FULL_SENTENCE_DEFINITION: function (state, payload) {
            Vue.set(state, 'latestFullSentenceDefinition', payload.fsd);
        },
        SET_FINAL_FULL_SENTENCE_DEFINITION: function (state, payload) {
            Vue.set(state, 'finalFullSentenceDefinition', payload.fsd);
        },
        ADD_MESSAGE: (state, payload) => {
            let verb = payload.verb;
            if (!verb.messages) {
                verb.messages = [] as Message[];
            }
            const message = {
                verbId: verb.id,
                user: state.currentUser.username,
                content: payload.messageContent
            };
            verb.messages.unshift(message);
        },
        REMOVE_MESSAGE: (state, payload) => {
            payload.verb.messages.splice(payload.index, 1);
        },
        MARK_MESSAGES_AS_READ: (state, payload) => {
            const messages = payload.verb.messages as Message[];
            messages.forEach(message => {
                let users = message.readBy ? message.readBy.split(',') : null;
                if (!_.includes(users, payload.username)) {
                    if (!users) {
                        users = [];
                    }
                    users.push(payload.username);
                }
                message.readBy = users.join(',');
            });
        },
        LINK_VERB: (state, payload: LinkPayload) => {
            let verb = payload.verb;
            let links = payload.group === 1 ? payload.verb.links1 : payload.verb.links2;
            if (!links) {
                links = [] as string[]; // list of verb guids
            }

            links.push(payload.linked.id);
        },
        UNLINK_VERB: (state, payload: LinkPayload) => {
            let links = payload.group === 1 ? payload.verb.links1 : payload.verb.links2;
            let index = links.findIndex(x => x === payload.linked.id);
            links.splice(index, 1);
        },
        SET_SUGGESTIONS: (state, data) => {
            Vue.set(state, 'suggestions', data.suggestions);
        },
        SET_FINAL_CHECK_LIST: (state, data) => {
            Vue.set(state, 'finalCheckList', data.finalCheckList);
        }
    },
    actions: {
        login({ dispatch, commit }, { username, password }): Promise<any> {
            const url = 'https://auth.dondoushidon.com/api/authenticate'
            //const url = 'http://localhost:8999/api/authenticate';
            const body = 'username=' + username + '&password=' + password

            return axios.post(url, body, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                .then(response => {
                    // success callback
                    commit('SET_USER', response.data);
                    return response.data;
                }, response => {
                    // error callback
                    commit('SET_USER', null);
                    if (response.status === 401) {
                        throw new Error('Bad credentials')
                    } else {
                        return response.json()
                    }
                });
        },
        logout({ commit }) {
            commit('SET_USER', null);
        },
        loadData({ dispatch, commit }) {
            dispatch('loadAllUsers').then(() => {
                dispatch('loadJpMembers').then(() => {
                    dispatch('loadEnMembers').then(() => {
                        dispatch('loadManagers').then(() => {
                            dispatch('loadStageGroups').then(() => {
                                dispatch('loadVerbList').then(response => { });
                            });
                        });
                    });
                });
            }, (error) => { commit('SET_USER', null); });
        },
        loadAllUsers({ commit }) {
            const token = localStorage.getItem('d3');
            axios.get('/api/users?token=' + token).then((response) => {
                var members = response.data;
                console.log(JSON.stringify(members));
                commit('SET_USERS', members)
            }, (error) => {
                commit('SET_USER', null);
                window.location.reload();
            });
        },
        loadJpMembers({ commit }) {
            const token = localStorage.getItem('d3');
            axios.get('/api/jpMembers?token=' + token).then((response) => {
                var members = response.data;
                members.push({
                    userId: '00000000-0000-0000-0000-000000000000',
                    displayName: '未設定',
                    username: null
                });
                console.log(JSON.stringify(members));
                commit('SET_JP_MEMBERS', members)
            }, (error) => {
                commit('SET_USER', null);
                window.location.reload();
            });

            axios.get('/api/jpManagers?token=' + token).then((response) => {
                var managers = response.data;
                managers.push({
                    userId: '00000000-0000-0000-0000-000000000000',
                    displayName: '未設定',
                    username: null
                });
                commit('SET_JP_MANAGERS', managers)
            }, (error) => {
                commit('SET_USER', null);
                window.location.reload();
            });
        },
        loadEnMembers({ commit }) {
            const token = localStorage.getItem('d3');
            axios.get('/api/enMembers?token=' + token).then((response) => {
                var members = response.data;
                members.push({
                    userId: '00000000-0000-0000-0000-000000000000',
                    displayName: '未設定',
                    usernsame: null
                });
                console.log(JSON.stringify(members));
                commit('SET_EN_MEMBERS', members)
            }, (error) => {
                commit('SET_USER', null);
                window.location.reload();
            });

            axios.get('/api/enManagers?token=' + token).then((response) => {
                var managers = response.data;
                managers.push({
                    userId: '00000000-0000-0000-0000-000000000000',
                    displayName: '未設定',
                    username: null
                });
                commit('SET_EN_MANAGERS', managers)
            }, (error) => {
                commit('SET_USER', null);
                window.location.reload();
            });
        },
        loadManagers({ commit }) {
            const token = localStorage.getItem('d3');
            axios.get('/api/managers?token=' + token).then((response) => {
                var managers = response.data;
                managers.push({
                    userId: '00000000-0000-0000-0000-000000000000',
                    displayName: '未設定',
                    username: null
                });
                commit('SET_MANAGERS', managers)
            }, (error) => {
                commit('SET_MANAGERS', null);
                window.location.reload();
            });
        },
        loadStageGroups({ commit }) {
            const token = localStorage.getItem('d3');
            axios.get('/api/stageGroups?token=' + token).then((response) => {
                var groups = response.data;
                commit('SET_STAGE_GROUPS', groups)
            }, (error) => {
                commit('SET_STAGE_GROUPS', null);
                window.location.reload();
            });
        },
        loadVerbList({ commit }) {
            var stageQuery = '/0';
            if (store.state.mode === 'stage2') {
                stageQuery = '/2';
            }
            if (store.state.mode === 'stage6') {
                stageQuery = '/6';
            }
            if (store.state.mode === 'normal' && store.state.filterStage > 0) {
                stageQuery = '/' + store.state.filterStage;
            }
            var turnQuery = '';
            if (store.state.mode === 'normal' && store.state.filterTurn != null && store.state.filterTurn !== '') {
                turnQuery = '/' + store.state.filterTurn;
            }

            var searchQuery = '';
            var page = store.state.page;
            if (store.state.searchWord) {
                searchQuery += '?search=' + store.state.searchWord;
                page = 0;
            }

            const token = localStorage.getItem('d3');
            var tokenQuery = searchQuery ? `&token=${token}` : `?token=${token}`;
            const url = `/api/verbs/${page}/${store.state.numItemsPerPage}${stageQuery}${turnQuery}${searchQuery}${tokenQuery}`;
            axios.get(url).then((response) => {
                var verbs = response.data;
                console.log(JSON.stringify(verbs));
                commit('SET_VERBS', verbs);
            });
        },
        openEditor({ commit }, { verb }) {
            commit('SET_CAN_CHANGE_STAGE');
            commit('SET_ACTIVE_VERB', verb);
        },
        setCurrentStage({ commit }, { stage }) {
            commit('SET_CURRENT_STAGE', stage);
        },
        loadExamples({ commit }, { index, lang, verbId, localId, stage }) {
            axios.get(`/api/examples/${lang}/verbId/${verbId}/localId/${localId}/stage/${stage}`)
                .then((response) => {
                    let anySelected = false;
                    let examples = Object.assign([], response.data);
                    console.log('RESPONSE from server: ' + JSON.stringify(examples));
                    let selectedIndex;
                    _.each(examples, (ex, index) => {
                        if (ex.selected) {
                            anySelected = true;
                            selectedIndex = index;
                        }
                    });
                    if (lang === 1 && stage === 3 && !anySelected ||
                        lang === 2 && stage === 7 && !anySelected) {
                        commit('RESET_CAN_CHANGE_STAGE');
                    }
                    let data = {
                        list: examples,
                        selectedIndex: selectedIndex
                    }
                    const action = lang === 1 ? 'SET_JP_EXAMPLES' : 'SET_EN_EXAMPLES';
                    commit(action, { index, localId, data });
                    _.each(examples, (ex) => {
                        if (ex.id) {
                            axios.get('/api/comments/' + ex.id).then((response) => {
                                commit('SET_COMMENTS', { linkId: ex.id, comments: response.data })
                            });
                        }
                    });
                });
        },
        loadFullSentenceDefinitions({ commit }, { verbId, stage }) { // stage must be 7
            axios.get('/api/fsds/verbId/' + verbId + '/stage/' + stage).then((response) => {
                let fsds = Object.assign([], response.data);
                console.log('RESPONSE from server: ' + JSON.stringify(fsds));
                let data;
                // If any saved in stage 7, just take them, otherwise take the first one, which is saved in the highest stage.
                if (_.some(fsds, (f) => +f.stage === 7)) {
                    let fsds7 = _.filter(fsds, { stage: 7 });
                    let selectedIndex;
                    _.each(fsds7, (fsd, index) => {
                        if (fsd.selected) {
                            selectedIndex = index;
                        }
                    });

                    data = {
                        list: fsds7,
                        selectedIndex: selectedIndex
                    };
                } else { // none saved in stage 7, take the one saved in the highest stage.
                    data = {
                        list: fsds && fsds[0] ? [fsds[0]] : [],
                        selectedIndex: 0
                    };
                }

                commit('SET_FULL_SENTENCE_DEFINITIONS', data);
                _.each(fsds, (fsd) => {
                    axios.get('/api/comments/' + fsd.id).then((response) => {
                        commit('SET_COMMENTS', { linkId: fsd.id, comments: response.data })
                    })
                });
            });
        },
        loadFullSentenceDefinition({ commit }, { verbId, stage }) {
            axios.get('/api/fsd/verbId/' + verbId + '/stage/' + stage).then((response) => {
                let fsd = Object.assign({}, response.data);
                console.log('loadFullSentenceDefinition RESPONSE from server: ' + JSON.stringify(fsd));

                commit('SET_FULL_SENTENCE_DEFINITION', { fsd });
            });
        },
        loadFinalFullSentenceDefinition({ commit }, { verbId }) {
            axios.get('/api/finalFsd/verbId/' + verbId).then((response) => {
                let fsd = Object.assign({}, response.data);
                console.log('Final FullSentenceDefinition RESPONSE from server: ' + JSON.stringify(fsd));

                commit('SET_FINAL_FULL_SENTENCE_DEFINITION', { fsd });
            });
        },//???
        loadSuggestions({ commit }, { verbId }) {
            axios.get(`/api/suggestions/verbId/${verbId}`).then((response) => {
                let initialSuggestions: Suggestion = {
                    id: '',
                    fullSentenceDefinition: '',
                    translations: ['', '', '', ''],
                    noteForReferenceBook: ''
                };
                let suggestions: Suggestion = Object.assign(initialSuggestions, response.data);
                console.log('suggestions RESPONSE from server: ' + JSON.stringify(suggestions));

                commit('SET_SUGGESTIONS', { suggestions });
            });
        },
        loadFinalCheckList({ commit }, { verbId }) {
            axios.get(`/api/finalCheckList/verbId/${verbId}`).then((response) => {
                let initialCheckList: FinalCheckList = {
                    kanji: false,
                    englishIndex: false,
                    link1: false,
                    link2: false
                };
                let finalCheckList: FinalCheckList = Object.assign(initialCheckList, response.data);
                console.log('initialCheckList RESPONSE from server: ' + JSON.stringify(initialCheckList));

                commit('SET_FINAL_CHECK_LIST', { finalCheckList });
            });
        },
        addNewEntry({ dispatch, commit }, { verbText }) {
            let newVerb = { value: verbText };
            axios.post('/api/addVerb', newVerb).then((response) => {
                let page = response.data;
                commit('CHANGE_PAGE', page);
                dispatch('loadVerbList');
            });
        },
        changeMode({ dispatch, commit }, { mode }) {
            commit('CHANGE_MODE', mode);
            commit('CHANGE_PAGE', 0); // go back to the first page to show the matches from the beginning
            dispatch('loadVerbList');
        },
        filterByStage({ dispatch, commit }, { stage }) {
            commit('FILTER_BY_STAGE', stage);
            commit('CHANGE_PAGE', 0); // go back to the first page to show the matches from the beginning
            dispatch('loadVerbList');
        },
        filterByTurn({ dispatch, commit }, turn) {
            commit('FILTER_BY_TURN', turn);
            commit('CHANGE_PAGE', 0); // go back to the first page to show the matches from the beginning
            dispatch('loadVerbList');
        },
        goToPrevPage({ dispatch, commit }) {
            if (store.state.page > 0) {
                commit('GO_TO_PREV_PAGE');
                dispatch('loadVerbList');
            }
        },
        goToNextPage({ dispatch, commit }) {
            if (store.state.page < Math.floor(store.state.numVerbs / store.state.numItemsPerPage)) {
                commit('GO_TO_NEXT_PAGE');
                dispatch('loadVerbList');
            }
        },
        search({ dispatch, commit }, { word }) {
            commit('SEARCH', word);
            dispatch('loadVerbList');
            //axios.get('/api/verbs/' + word).then((response) => {
            //  var doc = response.data;
            //  console.log(JSON.stringify(doc));
            //  commit('SET_VERBS', doc)
            //});
        },
        updateEntry({ commit }, { verb, previousStage }) {
            if (store.state.mode === 'stage2' && verb.stage === 2 && verb.jpAssigneeId !== '00000000-0000-0000-0000-000000000000') {
                verb.stage = 3;
            }
            if (store.state.mode === 'stage6' && verb.stage === 6 && verb.enAssigneeId !== '00000000-0000-0000-0000-000000000000') {
                verb.stage = 7;
            }

            if (verb.stage === 1) {
                commit('SET_CAN_CHANGE_STAGE');
            }

            axios.post(`/api/updateVerb?previousStage=${previousStage}`, verb).then((response) => {
                commit('UPDATE_ENTRY', response.data as Verb);
                if (previousStage !== undefined) {
                    const updated = store.state.verbs.find(v => v.id === verb.id);
                    updated.selected = false; // changing state closes dialog
                }
            });
        },
        changeStage({ dispatch, commit }, { verb, previousStage }) {
            verb.selected = false;
            dispatch('updateEntry', { verb, previousStage });
        },
        // login({ dispatch, commit }, { username, password }) {
        //     const url = 'https://auth-server-41.herokuapp.com/api/authenticate'
        //     let body = 'username=' + username + '&password=' + password

        //     axios.post(url, body, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(response => {
        //         // success callback
        //         commit('SET_USER', response.data);
        //         dispatch('loadData');
        //     }, response => {
        //         // error callback
        //         commit('SET_USER', null);
        //         if (response.status === 401) {
        //             throw new Error('Bad credentials')
        //         } else {
        //             return response.json()
        //         }
        //     });
        // },
        // logout({ commit }) {
        //     commit('SET_USER', null);
        // },
        // loadData({ dispatch }) {
        //     dispatch('loadJpMembers').then(() => {
        //         dispatch('loadEnMembers').then(() => {
        //             dispatch('loadVerbList').then(response => { });
        //         })
        //     })
        // },
        // loadJpMembers({ commit }) {
        //     axios.get('/api/jpMembers').then((response) => {
        //         var members = response.data;
        //         console.log(JSON.stringify(members));
        //         commit('SET_JP_MEMBERS', members)
        //     });
        // },
        // loadEnMembers({ commit }) {
        //     axios.get('/api/enMembers').then((response) => {
        //         var members = response.data;
        //         console.log(JSON.stringify(members));
        //         commit('SET_EN_MEMBERS', members)
        //     });
        // },
        // loadVerbList({ commit }) {
        //     var stageQuery = '/0';
        //     if (store.state.mode === 'stage2') {
        //         stageQuery = '/2';
        //     }
        //     if (store.state.mode === 'stage6') {
        //         stageQuery = '/6';
        //     }

        //     var searchQuery = '';
        //     if (store.state.searchWord) {
        //         searchQuery = '/' + store.state.searchWord;
        //     }

        //     axios.get('/api/verbs/' + store.state.page + '/' + store.state.numItemsPerPage + stageQuery + searchQuery).then((response) => {
        //         var verbs = response.data;
        //         console.log(JSON.stringify(verbs));
        //         commit('SET_VERBS', verbs);
        //     });
        // },
        // openEditor({ commit }, { verb }) {
        //     commit('SET_CAN_CHANGE_STAGE');
        //     commit('SET_ACTIVE_VERB', verb);
        // },
        deleteEntry({ commit }, { verb, index }) {
            verb.selected = false; // close the editor
            verb.status = 1; /* Deleted */

            axios.post('/api/updateVerb', verb).then((response) => {
                commit('DELETE_ENTRY', { verb, index });
            });
        },
        cloneEntry({ commit }, { verb, index, examples, keepExampleSrc, keepExampleDst }) {
            verb.selected = false; // close the editor
            let clone = JSON.parse(JSON.stringify(verb));

            axios.post('/api/cloneVerb', verb).then((response) => {
                let clonedVerb = JSON.parse(JSON.stringify(response.data));
                commit('CLONE_ENTRY', { verb: clonedVerb, index });

                for (let i = 0; i < 4; i++) {
                    if (examples[i]) {
                        let clonedExample = JSON.parse(JSON.stringify(examples[i]));
                        if (!keepExampleSrc[i]) { // Delete src
                            examples[i].status = 1; // DELETED
                            axios.post('/api/updateExample', examples[i])
                        }
                        if (keepExampleDst[i]) { // Copy across to dst
                            clonedExample.id = null; // to create a new one
                            clonedExample.verbId = clonedVerb.id;
                            axios.post('/api/updateExample', clonedExample);
                        }
                    }
                }
            });
        },
        // loadExamples({ commit }, { lang, verbId, localId, stage }) {
        //     axios.get('/api/examples/' + lang + '/verbId/' + verbId + '/localId/' + localId + '/stage/' + stage).then((response) => {
        //         let anySelected = false;
        //         let examples = Object.assign([], response.data);
        //         console.log('RESPONSE from server: ' + JSON.stringify(examples));
        //         let selectedIndex;
        //         _.each(examples, (ex, index) => {
        //             if (ex.selected) {
        //                 anySelected = true;
        //                 selectedIndex = index;
        //             }
        //         });
        //         if ((stage !== 1 || lang === 1 && stage === 3 || lang === 2 && stage === 7) && !anySelected) {
        //             commit('RESET_CAN_CHANGE_STAGE');
        //         }
        //         let data = {
        //             list: examples,
        //             selectedIndex: selectedIndex
        //         }
        //         const action = lang === 1 ? 'SET_JP_EXAMPLES' : 'SET_EN_EXAMPLES';
        //         commit(action, { localId, data });
        //         _.each(examples, (ex) => {
        //             axios.get('/api/comments/' + ex.id).then((response) => {
        //                 commit('SET_COMMENTS', { linkId: ex.id, comments: response.data })
        //             })
        //         });
        //     });
        // },
        // loadFullSentenceDefinitions({ commit }, { verbId, stage }) { // stage must be 7
        //     axios.get('/api/fsds/verbId/' + verbId + '/stage/' + stage).then((response) => {
        //         let fsds = Object.assign([], response.data);
        //         console.log('RESPONSE from server: ' + JSON.stringify(fsds));
        //         let data;
        //         // If any saved in stage 7, just take them, otherwise take the first one, which is saved in the highest stage.
        //         if (_.some(fsds, (f) => +f.stage === 7)) {
        //             let fsds7 = _.filter(fsds, { stage: 7 });
        //             let selectedIndex;
        //             _.each(fsds7, (fsd, index) => {
        //                 if (fsd.selected) {
        //                     selectedIndex = index;
        //                 }
        //             });

        //             data = {
        //                 list: fsds7,
        //                 selectedIndex: selectedIndex
        //             };
        //         } else { // none saved in stage 7, take the one saved in the highest stage.
        //             data = {
        //                 list: fsds && fsds[0] ? [fsds[0]] : [],
        //                 selectedIndex: 0
        //             };
        //         }

        //         commit('SET_FULL_SENTENCE_DEFINITIONS', data);
        //         _.each(fsds, (fsd) => {
        //             axios.get('/api/comments/' + fsd.id).then((response) => {
        //                 commit('SET_COMMENTS', { linkId: fsd.id, comments: response.data })
        //             })
        //         });
        //     });
        // },
        loadCandidates({ commit }, { verbId, stage, apiPath, mutation }) {
            axios.get(apiPath + '/verbId/' + verbId + '/stage/' + stage).then((response) => {
                let candidates = Object.assign([], response.data);
                console.log('candidates RESPONSE from server: ' + JSON.stringify(candidates));
                let data;
                // If any saved in stage, just take them, otherwise take the first one, which is saved in the highest stage.
                if (_.some(candidates, (f) => +f.stage === stage)) {
                    let candidatesInPreviousStage = _.filter(candidates, { stage });
                    let selectedIndex;
                    _.each(candidatesInPreviousStage, (c, index) => {
                        if (c.selected) {
                            selectedIndex = index;
                        }
                    });

                    data = {
                        list: candidatesInPreviousStage,
                        selectedIndex: selectedIndex
                    };
                } else { // none saved in stage, take the one saved in the highest stage.
                    data = {
                        list: candidates && candidates[0] ? [candidates[0]] : [],
                        selectedIndex: -1
                    };
                }

                commit(mutation, data);

                _.each(candidates, (c) => {
                    axios.get('/api/comments/' + c.id).then((response) => {
                        commit('SET_COMMENTS', { linkId: c.id, comments: response.data })
                    })
                });
            });
        },
        addExample({ commit }, { index, lang, verbId, stage, localId }) {
            commit('ADD_EXAMPLE', { index, lang, verbId, stage, localId });
        },
        deleteExample({ commit }, { index, example, localId }) {
            if (example.created) {
                example.status = 1; // 1: DELETED
                axios.post('/api/updateExample', example).then((response) => {
                    console.log(JSON.stringify(response));
                    commit('DELETE_EXAMPLE', { index, example, localId });
                    commit('UPDATE_CAN_CHANGE_STAGE', example.language);
                });
            } else {
                commit('DELETE_EXAMPLE', { index, example, localId });
                commit('UPDATE_CAN_CHANGE_STAGE', example.language);
            }
        },
        addComment({ commit }, { linkId }) {
            commit('ADD_COMMENT', linkId);
        },
        saveExample({ commit }, { index, ex, localId }) {
            axios.post('/api/updateExample', ex).then((response) => {
                commit('UPDATE_EXAMPLE', { index, example: response.data, localId });
            });
        },
        saveComment({ commit }, { comment, index }) {
            axios.post('/api/updateComment', comment).then((response) => {
                console.log(JSON.stringify(response));
                commit('UPDATE_COMMENT', { comment: response.data, index: index });
            });
        },
        saveBestChoice({ commit }, { index, examples, selectedIndex }) {
            _.each(examples, (example, localId: number) => {
                example.selected = localId === selectedIndex
                commit('UPDATE_EXAMPLE', { index, example, localId });
            });
            axios.post('/api/updateExamples', examples).then((response) => {
                commit('SET_CAN_CHANGE_STAGE');
            });
        },
        deleteComment({ commit }, { comment, index }) {
            if (comment.created) {
                comment.status = 1; // 1: DELETED
                axios.post('/api/updateComment', comment).then((response) => {
                    console.log(JSON.stringify(response));
                    commit('DELETE_COMMENT', { comment, index });
                });
            } else {
                commit('DELETE_COMMENT', { comment, index });
            }
        },
        deleteCandidate({ commit }, { verb, type, candidate, index }) {
            if (candidate.created) {
                candidate.status = 1; // 1: DELETED
                axios.post('/api/update' + type, candidate).then((response) => {
                    console.log(JSON.stringify(response));
                    commit('DELETE_CANDIDATE', { candidate, index });
                    if (candidate.selected && type === 'ReferenceBookNote') {
                        verb.noteForReferenceBook = '';
                        axios.post('/api/updateVerb', verb).then((response) => {
                            commit('UPDATE_ENTRY', verb);
                        });
                    }
                });
            } else {
                commit('DELETE_CANDIDATE', { candidate, index });
            }
        },
        addCandidate({ commit }, { verbId, stage }) {
            commit('ADD_CANDIDATE', { verbId, stage });
        },
        saveCandidate({ commit }, { name, candidate, index }) {
            axios.post('/api/update' + name, candidate).then((response) => {
                commit('UPDATE_CANDIDATE', { candidate: response.data, index: index });
            });
        },
        selectCandidate({ commit }, { verb, type, candidateList, selectedIndex }) {
            _.each(candidateList, (candidate, index) => {
                candidate.selected = index === selectedIndex
                commit('UPDATE_CANDIDATE', { candidate, index });
                if (candidate.selected && type === 'ReferenceBookNote') {
                    verb.noteForReferenceBook = candidate.value;
                    commit('UPDATE_ENTRY', verb);
                }
            });
            axios.post('/api/update' + type + 's', candidateList).then((response) => {
            });
        },
        // loadFullSentenceDefinition({ commit }, { verbId, stage }) {
        //     axios.get('/api/fsd/verbId/' + verbId + '/stage/' + stage).then((response) => {
        //         let fsd = Object.assign({}, response.data);
        //         console.log('loadFullSentenceDefinition RESPONSE from server: ' + JSON.stringify(fsd));

        //         commit('SET_FULL_SENTENCE_DEFINITION', { fsd });
        //     });
        // },
        // loadFinalFullSentenceDefinition({ commit }, { verbId }) {
        //     axios.get('/api/finalFsd/verbId/' + verbId).then((response) => {
        //         let fsd = Object.assign({}, response.data);
        //         console.log('Final FullSentenceDefinition RESPONSE from server: ' + JSON.stringify(fsd));

        //         commit('SET_FINAL_FULL_SENTENCE_DEFINITION', { fsd });
        //     });
        // },
        updateFullSentenceDefinition({ commit }, { fsd }) {
            axios.post('/api/updateFullSentenceDefinition', fsd)
        },
        loadReferenceBookNotes({ dispatch }, { verbId, stage }) {
            dispatch('loadCandidates', { verbId, stage, apiPath: '/api/refBookNotes', mutation: 'SET_REFERENCE_BOOK_NOTES' });
        },
        addMessage({ commit }, { verb, messageContent }) { // used for adding messages
            commit('ADD_MESSAGE', { verb, messageContent });
        },
        removeMessage({ commit }, { verb, index }) {
            commit('REMOVE_MESSAGE', { verb, index });
        },
        markMessagesAsRead({ commit }, { verb, username }) {
            commit('MARK_MESSAGES_AS_READ', { verb, username });
            axios.post('/api/updateVerb', verb).then((response) => {
                commit('UPDATE_ENTRY', verb);
            });
        },
        linkVerb({ commit }, { verb, linked, group }: LinkPayload) {
            commit('LINK_VERB', { verb, linked, group });
        },
        unlinkVerb({ commit }, { verb, linked, group }: LinkPayload) {
            commit('LINK_VERB', { verb, linked, group });
        },
        updateSuggestions({ commit }, { verbId }) {
            axios.post(`/api/updateSuggestions/verbId/${verbId}`, store.state.suggestions);
        },
        updateFinalCheckList({ commit }, { verbId }) {
            axios.post(`/api/finalCheckList/verbId/${verbId}`, store.state.finalCheckList);
        }
    }
});

export default store