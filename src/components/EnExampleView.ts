import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator';

import SentenceEditor from './SentenceEditor.vue';
import store from '../store';

import * as _ from 'lodash';


@Component({
    name: 'en-example-view',
    components: {
        SentenceEditor
    }
})
export default class EnExampleView extends Vue {
    public get $store() { return store }

    get verbId() { return this.verb.id; }
    get stage() { return this.verb.stage; }
    get examples() {
        return this.$store.state.enExamples[this.index]
            ? this.$store.state.enExamples[this.index][this.localId]
            : [];
    }
    get jpExamples() {
        return this.$store.state.jpExamples[this.index]
            ? this.$store.state.jpExamples[this.index][this.localId]
            : [];
    }
    get fsd() {
        let fsd = _.find(this.$store.state.candidates.list, 'selected');

        return fsd ? fsd.value : '';
    }

    @Prop()
    verb: any;
    @Prop()
    index: number;
    @Prop()
    localId: number;

    created() {
        console.log('EnExampleView - localId: ' + this.localId + ', verbId: ' + this.verbId);
        this.$store.dispatch('loadExamples', {
            index: this.index,
            lang: 1,
            localId: this.localId,
            verbId: this.verbId,
            stage: this.verb.stage
        });
        this.$store.dispatch('loadExamples', {
            index: this.index,
            lang: 2,
            localId: this.localId,
            verbId: this.verbId,
            stage: this.verb.stage
        });
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }
}

Vue.component('en-example-view', EnExampleView)