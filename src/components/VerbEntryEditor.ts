import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import SettingsEditor from './SettingsEditor.vue'
import EntryEditor from './EntryEditor.vue'
import JpExampleView from './JpExampleView.vue'
import EnExampleView from './EnExampleView.vue'
import SummaryView from './SummaryView.vue'
import FullSentenceDefinitionView from './FullSentenceDefinitionView.vue'
import ReferenceBookNoteView from './ReferenceBookNoteView.vue'
import FinalCheckList from './FinalCheckList.vue'
import store from '../store'
import { VueTabs, VTab } from 'vue-nav-tabs'
import CompletedPanel from './CompletedPanel.vue'
import StagePanel from './StagePanel.vue';

// import VerbEntryEditor from './VerbEntryEditor'
// import JpAssigneeSelector from './JpAssigneeSelector'
// import EnAssigneeSelector from './EnAssigneeSelector'
// import WrapFilter from '../filters/WrapFilter'
import { modal, spinner } from 'vue-strap'
import { Verb } from "../models/Verb";

@Component({
    name: 'verb-entry-editor',
    components: {
        SettingsEditor,
        EntryEditor,
        JpExampleView,
        EnExampleView,
        SummaryView,
        FullSentenceDefinitionView,
        ReferenceBookNoteView,
        FinalCheckList,
        VueTabs,
        VTab,
        CompletedPanel,
        spinner,
        StagePanel
    }
})
export default class VerbEntryEditor extends Vue {
    public get $store() { return store; }

    private get userRoles(): string[] {
        return this.$store.state.currentUser.roles;
    }

    public get isUserAdmin() {
        return this.$store.state.currentUser.roles.findIndex(x => x == 'admin') >= 0;
    }

    public get verbId() {
        return this.verb.id;
    }

    public get stage() {
        return +this.verb.stage;
    }

    @Prop()
    verb: Verb;
    @Prop()
    index: number;
    @Prop()
    jpMembers: any[];
    @Prop()
    enMembers: any[];
    @Prop()
    jpManagers: any[];
    @Prop()
    enManagers: any[];

    @Watch('jpManagers')
    onJpManagersChanged(value): void {
        if (value && this.verb) {
            this.verb.turnId = this.jpManagers[0].id; // assuming there's only one manager
        }
    }

    @Watch('enManagers')
    onEnManagersChanged(value): void {
        if (value && this.verb) {
            this.verb.turnId = this.enManagers[0].id; // assuming there's only one manager
        }
    }

    created() {
        this.$store.dispatch('openEditor', { verb: this.verb });
    }
}

Vue.component('verb-entry-editor', VerbEntryEditor)