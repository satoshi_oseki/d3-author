import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator';
import ClickConfirm from 'click-confirm';
import Popper from 'vue-popperjs';

import { Message } from './../models/Message';
import store from '../store';

import * as _ from 'lodash';
import moment from 'moment';

@Component({
    name: 'message-view',
    components: {
        ClickConfirm,
        Popper
    }
})
export default class MessageView extends Vue {
    private editingMessage: boolean = false;
    private messageContent: string = '';

    public get $store() { return store; }

    get verbId() { return this.verb.id; }
    get stage() { return this.verb.stage; }
    get messages() { return this.verb.messages; }

    @Prop()
    editable: boolean;
    @Prop()
    verb: any;

    constructor() {
        super();
    }

    created() {
    }

    messageUnread(message: Message) {
        if (!message.readBy) {
            return true;
        }
        const user = this.$store.state.currentUser.username;
        var usersWhoReadMessage = message.readBy.split(',');
        return !_.includes(usersWhoReadMessage, user);
    }

    getTimestamp(message: Message): string {
        const messageTime = message.created;
        return messageTime != null ?
            moment.utc(messageTime).local().format('YYYY/MM/DD HH:mm') :
            'not submitted';
    }

    startEditing(e): void {
        this.editingMessage = true;
    }

    addMessage(e): void {
        this.$store.dispatch('addMessage', { verb: this.verb, messageContent: this.messageContent });
        this.editingMessage = false;
        this.messageContent = '';
    }

    removeMessage(index): void {
        this.$store.dispatch('removeMessage', { verb: this.verb, index });
    }

    hideMessageBox(e): void {
        this.messageContent = '';
        this.editingMessage = false;
    }
}

Vue.component('message-view', MessageView)