import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import ExampleMapView from './ExampleMapView.vue'
import CandidatesEditor from './CandidatesEditor.vue'

import store from '../store'

import * as _ from 'lodash'

@Component({
    name: 'full-sentence-definition-view',
    components: {
        ExampleMapView,
        CandidatesEditor
    }
})
export default class FullSentenceDefinitionView extends Vue {
    candidatesRef: any;

    public get $store() { return store }

    get verbId() { return this.verb.id; }

    get stage() { return this.verb.stage; }

    get fsd() { return _.find(this.$store.state.candidates.list, 'selected'); }

    @Prop()
    index: number;
    @Prop()
    verb: any;
    @Prop()
    localId: number;

    constructor() {
        super();
        this.candidatesRef = this.$store.state.candidates;
    }

    created() {
        console.log('FullSentenceDefinitionView - localId: ' + this.localId + ', verbId: ' + this.verbId);
        this.$store.dispatch('loadFullSentenceDefinitions', { verbId: this.verbId, stage: this.verb.stage });
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }
}

Vue.component('full-sentence-definition-view', FullSentenceDefinitionView)