import Vue from "vue";
import { Component, Prop, Watch, Provide, Model } from 'vue-property-decorator'

import store from '../store'

import * as _ from 'lodash'

@Component({
    name: 'autosize-textare',
    components: {
    }
})
export default class AutosizeTextarea extends Vue {
    @Model('change')
    @Prop({ required: false })
    value: string;

    mounted() {
        var el = this.$refs.textarea as HTMLElement;
        el.onfocus = (event) => this.onFocus(event);
    }

    onFocus(event) {
        this.adjustHeight(event.currentTarget);
    }

    onKeyup(event) {
        event.currentTarget.style.height = '1px';
        var offset = event.currentTarget.offsetHeight - event.currentTarget.clientHeight;
        const height = event.currentTarget.scrollHeight + offset;
        event.currentTarget.style.height = `${height}px`;
    }

    adjustHeight(element) {
        element.focus();
        element.dispatchEvent(new Event('keyup'));
    }
}

Vue.component('autosize-textarea', AutosizeTextarea)