import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'
import { User } from "../models/User";
import { Verb } from "../models/Verb";

@Component({
    name: 'jp-assignee-selector',
    components: {
    }
})
export default class JpAssigneeSelector extends Vue {
    public get $store() { return store; }
    public get authorInputName() { return `author-${this.verb.id}`; }
    public get checkerInputName() { return `checker-${this.verb.id}`; }

    @Prop()
    verb: Verb;
    @Prop()
    jpMembers: any;
    @Prop()
    jpManagers: any;

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
        this.$store.dispatch('loadVerbList');
    }
}

Vue.component('jp-assignee-selector', JpAssigneeSelector)