import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator';
import Popper from 'vue-popperjs';

import * as _ from 'lodash';
import store from '../store';
import MessageView from './MessageView.vue';
import { Verb } from './../models/Verb';

@Component({
    name: 'message-popover',
    components: {
        Popper,
        MessageView
    }
})
export default class MessagePopover extends Vue {
    public get $store() { return store; }

    get verbId() { return this.verb.id; }
    get stage() { return this.verb.stage; }
    get messages() { return this.verb.messages; }
    get numOfUnreadMessages(): number {
        if (!this.messages) { return 0; }
        let numOfUnread = 0;
        this.messages.forEach(message => {
            if (message.readBy == null ||
                !_.includes(message.readBy.split(','), this.$store.state.currentUser.username)) {
                numOfUnread++;
            }
        })
        return numOfUnread;
    }

    @Prop()
    verb: Verb;
    @Prop()
    editable: boolean;

    constructor() {
        super();
    }

    created() {
    }

    confirmRead(e) {
        (this.$refs['message-button'] as any).click(); // click mesage button to close the popover

        this.$store.dispatch('markMessagesAsRead', { verb: this.verb, username: this.$store.state.currentUser.username });
    }
}

Vue.component('message-popover', MessagePopover)