import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'

import * as _ from 'lodash'
import { Example } from "../models/Example";
import { Verb } from "../models/Verb";

@Component({
    name: 'example-map-view',
    components: {
    }
})
export default class ExampleMapView extends Vue {
    public get $store() { return store }
    private get jpExamples(): Example[] { return this.getSelectedExamples('jp'); }
    private get enExamples(): Example[] { return this.getSelectedExamples('en'); }
    public get Examples() {
        let examples = [];
        for (let i = 0; i < 4; i++) {
            let pair = [];
            if (this.jpExamples) {
                pair.push(this.jpExamples[i]);
            }
            if (this.enExamples) {
                pair.push(this.enExamples[i]);
            }
            examples.push(pair);
        }

        return examples;
    }

    @Prop()
    index: number;
    @Prop()
    verb: Verb;

    getSelectedExamples(language) { // 'jp' | 'en'
        let examples = [];

        for (var i = 0; i < 4; i++) {
            const exampleList = _.get(this.$store.state, `${language}Examples[${this.index}][${i}].list`);
            let selected = _.filter(exampleList, 'selected');
            if (selected && selected.length > 0) {
                examples.push(selected[0]);
            } else {
                examples.push(null);
            }
        }

        return examples;
    }
}

Vue.component('example-map-view', ExampleMapView)