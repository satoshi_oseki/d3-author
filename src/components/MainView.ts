import VerbList from './VerbList.vue';
import Vue from "vue";
import Component from "vue-class-component";

import store from '../store'
import HeaderBar from './HeaderBar.vue';

@Component({
    name: "MainView",
    components: {
        VerbList, HeaderBar
    }
})
export default class MainView extends Vue {
    public get $store() { return store; }

    mounted(): void {
        if (this.$store.state.authUser) {
            this.$store.dispatch('loadData')
        }
    }
}