import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'

import * as _ from 'lodash'
import { Verb } from '../models/Verb';
import StageSelector from './StageSelector';

@Component({
    name: 'stage-panel',
    components: {
        StageSelector
    }
})
export default class StagePanel extends Vue {
    public get $store() { return store; }

    public get CanChangeStage(): boolean {
        return this.$store.state.canChangeStage
            && this.verb.stage !== this.$store.state.currentStage;
    }

    @Prop()
    verb: Verb;

    changeStage(): void {
        const previousStage = this.verb.stage;
        this.verb.stage = this.$store.state.currentStage;
        this.$store.dispatch('changeStage', { previousStage, verb: this.verb });
    }
}

Vue.component('stage-panel', StagePanel)
