import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'
import { Verb } from "../models/Verb";

@Component({
    name: 'en-assignee-selector',
    components: {
    }
})
export default class EnAssigneeSelector extends Vue {
    public get $store() { return store }
    public get authorInputName() { return `author-${this.verb.id}`; }
    public get checkerInputName() { return `checker-${this.verb.id}`; }

    @Prop()
    verb: Verb;
    @Prop()
    enMembers: any;
    @Prop()
    enManagers: any;

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
        this.$store.dispatch('loadVerbList');
    }
}

Vue.component('en-assignee-selector', EnAssigneeSelector)