﻿import Vue from "vue";
//import Component from "vue-class-component";
import store from '../store';
import { Component, Prop, Watch } from 'vue-property-decorator';
import { Filter } from 'vue-ts-decorate';

import VerbEntryEditor from './VerbEntryEditor.vue';
import JpAssigneeSelector from './JpAssigneeSelector.vue';
import EnAssigneeSelector from './EnAssigneeSelector.vue';
import ReferenceBookNoteAssigneeSelector from './ReferenceBookNoteAssigneeSelector.vue';
import { User } from './../models/User';
import { Verb } from './../models/Verb';
import WrapFilter from '../filters/WrapFilter';
import { modal, spinner } from 'vue-strap';
import MessagePopover from './MessagePopover.vue';
import axios from 'axios';

@Component({
    name: 'verb-list',
    components: {
        VerbEntryEditor,
        JpAssigneeSelector,
        EnAssigneeSelector,
        ReferenceBookNoteAssigneeSelector,
        modal,
        spinner,
        MessagePopover
    }
})
export default class VerbList extends Vue {
    @Prop()
    verbs: any[];

    private word: string;
    private showAddEntry: boolean;
    private newVerbText: string;
    private loaded: boolean;
    private filterStage: number;
    private filterTurn: string;
    private spinnerText: string;
    private exportChecker: number;
    private verb: Verb | null;

    public get $store() { return store; }
    private get turnOptions(): User[] {
        var off = { displayName: 'Off', userId: null } as User;
        return [off, ...this.$store.state.users];
    }

    constructor() {
        super();

        this.word = '';
        this.loaded = false;//?? this.$store.state.loaded.verbs;
        this.showAddEntry = false;
        this.newVerbText = '';
        this.filterStage = 0;
        this.filterTurn = null;
        this.spinnerText = 'Loading...'; // default
        this.verb = null;
    }

    @Watch('verbs')
    onVerbsChanged(value): void {
        if (value) {
            (this.$refs['loadingSpinner'] as any).hide();
        }
    }
    @Watch('filterStage')
    onFilterStageChanged(value): void {
        this.$store.dispatch('filterByStage', { stage: +value });
    }

    @Watch('filterTurn')
    onFilterTurnChanged(value): void {
        this.$store.dispatch('filterByTurn', value);
    }

    mounted(): void {
        (this.$refs['loadingSpinner'] as any).show();
    }

    addNewEntry(verbText): void {
        this.$store.dispatch('addNewEntry', { verbText });
        (this.$refs['loadingSpinner'] as any).show();
    }

    isEditable(verb): boolean {
        return this.$store.state.mode === 'normal' && verb.stage !== 2 && verb.stage !== 6;
    }

    goToPrevPage(): void {
        (this.$refs['loadingSpinner'] as any).show();
        this.$store.dispatch('goToPrevPage');
    }

    goToNextPage(): void {
        (this.$refs['loadingSpinner'] as any).show();
        this.$store.dispatch('goToNextPage');
    }

    showSpinner(text: string = 'Loading...'): void {
        this.spinnerText = text;
        (this.$refs['loadingSpinner'] as any).show();
    }

    hideSpinner(): void {
        (this.$refs['loadingSpinner'] as any).hide();
    }

    onEntryClicked(verb: Verb): void {
        if (!this.isEditable(verb)) {
            return;
        }
        verb.selected = true;
        this.verb = verb;
    }

    changeMode(mode): void {
        this.$store.dispatch('changeMode', { mode });
    }

    search(word: string): void {
        this.$store.dispatch('search', { word });
    }

    exportExcelFull() {
        this.showSpinner('Downloading...');
        const token = localStorage.getItem('d3');
        this.checkExportStatus(token);

        window.location.href = `/api/ExportExcelFull?token=${token}`;
    }

    exportExcel() {
        this.showSpinner('Downloading...');
        const token = localStorage.getItem('d3');
        this.checkExportStatus(token);

        window.location.href = `/api/ExportExcel?token=${token}`;
    }

    checkExportStatus(token: string) {
        this.exportChecker = setInterval(() => {
            axios.get(`/api/isExporting?token=${token}`).then((response) => {
                var exporting = response.data;
                if (!exporting) {
                    clearInterval(this.exportChecker);
                    this.hideSpinner();
                }
            });
        }, 1000)
    }
}

Vue.component('verb-list', VerbList)