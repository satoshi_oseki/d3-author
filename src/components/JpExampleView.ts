import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import SentenceEditor from './SentenceEditor.vue'
import store from '../store'

@Component({
    name: 'jp-example-view',
    components: {
        SentenceEditor
    }
})
export default class JpExampleView extends Vue {
    public get $store() { return store; }
    get verbId() { return this.verb.id; }
    get stage() { return this.verb.stage; }
    get examples() {
        return this.$store.state.jpExamples[this.index]
            ? this.$store.state.jpExamples[this.index][this.localId]
            : [];
    }

    @Prop()
    verb: any;
    @Prop()
    index: number;
    @Prop()
    localId: number;

    created() {
        console.log('JpExampleView - localId: ' + this.localId + ', verbId: ' + this.verbId)
        this.$store.dispatch('loadExamples', {
            index: this.index,
            lang: 1,
            localId: this.localId,
            verbId: this.verbId,
            stage: this.verb.stage
        });
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }
}

Vue.component('jp-example-view', JpExampleView)