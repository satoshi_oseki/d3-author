import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import { Example } from './../models/Example';
import MessageView from './MessageView.vue'
import store from '../store'
import { Verb } from '../models/Verb'

import * as _ from 'lodash'
import { User } from "../models/User";
import StageSelector from "./StageSelector.vue";
import TurnSelector from "./TurnSelector.vue";

@Component({
    name: 'final-check-list',
    components: {
        MessageView,
        StageSelector,
        TurnSelector
    }
})
export default class FinalCheckList extends Vue {
    currentStage: number;
    currentTurn: any;
    turnOptions: any[] = [];

    public get $store() { return store; }

    get verbId(): string {
        return this.verb.id;
    }

    get stage(): number {
        return this.verb ? +this.verb.stage : 0;
    }

    setTurnOptions(): void {
        this.turnOptions = [];

        let user = this.getUser('tanaka');
        this.turnOptions.push(user);
        this.turnOptions.push(this.$store.state.stageGroups.stage9group);
        this.currentTurn = this.verb.turnId;
    }

    getUser(username: string): User {
        return this.$store.state.users.find(x => x.username === username);
    }

    @Prop()
    verb: Verb;
    @Prop()
    jpMembers: any[];
    @Prop()
    enMembers: any[];
    @Prop()
    jpManagers: any[];
    @Prop()
    enManagers: any[];

    @Watch('currentStage')
    onCurrentStageChanged() { // Re-load the tab when stage changes.
        this.$store.dispatch('loadFullSentenceDefinition', { verbId: this.verbId, stage: this.currentStage })
    }

    @Watch('currentTurn')
    onCurrentTurnChanged() {
    }

    constructor() {
        super();
        this.currentStage = this.verb.stage;
        this.currentTurn = null;
    }

    created() {
        this.setTurnOptions();
        this.loadFinalCheckList(this.verbId);
    }

    loadFinalCheckList(verbId: string) {
        this.$store.dispatch('loadFinalCheckList', { verbId: this.verbId });
    }

    onSaveButtonClick(): void {
        this.updateEntry(this.verb);
        this.updateFinalCheckList();
    }

    updateEntry(verb): void {
        this.verb.stage = this.$store.state.currentStage;
        this.$store.dispatch('updateEntry', { verb });
    }

    updateFinalCheckList(): void {
        this.$store.dispatch('updateFinalCheckList', { verbId: this.verbId });
    }
}

Vue.component('final-check-list', FinalCheckList)