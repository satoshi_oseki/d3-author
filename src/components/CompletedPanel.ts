import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import { Example } from './../models/Example';
import MessageView from './MessageView.vue'
import store from '../store'
import { Verb } from '../models/Verb'

import * as _ from 'lodash'
import { User } from "../models/User";
import StageSelector from "./StageSelector.vue";
import TurnSelector from "./TurnSelector.vue";
import { FinalCheckList } from "../models/FinalCheckList";

@Component({
    name: 'final-check-list',
    components: {
        MessageView,
        StageSelector,
        TurnSelector
    }
})
export default class CompletedPanel extends Vue {
    public get $store() { return store; }

    get verbId(): string {
        return this.verb.id;
    }

    @Prop()
    verb: Verb;

    constructor() {
        super();
    }

    created() {
        this.loadFinalCheckList(this.verbId);
    }

    loadFinalCheckList(verbId: string) {
        this.$store.dispatch('loadFinalCheckList', { verbId: this.verbId });
    }

    onSaveButtonClick(): void {
        this.updateEntry(this.verb);
        this.resetFinalCheckList();
    }

    updateEntry(verb): void {
        this.verb.stage = this.$store.state.currentStage;
        this.$store.dispatch('updateEntry', { verb });
    }

    resetFinalCheckList(): void {
        if (this.$store.state.currentStage < 10) {
            this.$store.state.finalCheckList = {} as FinalCheckList; // Reset check list
            this.$store.dispatch('updateFinalCheckList', { verbId: this.verbId });
        }
    }
}

Vue.component('completed-panel', CompletedPanel)