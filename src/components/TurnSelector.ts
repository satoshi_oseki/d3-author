import Vue from "vue";
import { Component, Prop, Watch, Model } from 'vue-property-decorator'

import store from '../store'
import { Verb } from '../models/Verb'

import * as _ from 'lodash'
import { User } from "../models/User";

@Component({
    name: 'turn-selector'
})
export default class TurnSelector extends Vue {
    @Prop()
    verb: Verb;

    @Model('change')
    @Prop({ required: false })
    change: string;

    currentTurn: string;

    public get $store() { return store; }

    get stage(): number {
        return this.verb ? +this.verb.stage : 0;
    }

    public get availableTurns(): User[] {
        return this.getTurnOptions();
    }

    getTurnOptions(): User[] {
        let turnOptions = [] as User[];
        const currentStage = this.$store.state.currentStage;
        if (currentStage === 3 && this.$store.state.jpMembers) {
            const assignee = _.find(this.$store.state.jpMembers, m => m.userId === this.verb.jpAssigneeId)
            if (assignee) {
                turnOptions.push(assignee);
            }
            if (this.$store.state.jpManagers && this.$store.state.jpManagers.length > 0) {
                turnOptions.push(this.$store.state.jpManagers[0]);
            }

            if (this.verb.turnId == null && assignee) {
                this.verb.turnId = assignee.userId;
            }
            this.currentTurn = this.verb.turnId
        }

        if (currentStage === 4) {
            let user = this.getUser('hayashishita');
            turnOptions.push(user);
            this.currentTurn = user.userId;
            this.verb.turnId = this.currentTurn;
        }

        if (currentStage === 5) {
            turnOptions.push(this.$store.state.stageGroups.stage5group);
            this.currentTurn = this.$store.state.stageGroups.stage5group.userId;
            this.verb.turnId = this.currentTurn;
        }

        if (currentStage === 7 && this.$store.state.enMembers) {
            const assignee = _.find(this.$store.state.enMembers, m => m.userId === this.verb.enAssigneeId)
            if (assignee) {
                turnOptions.push(assignee);
            }
            if (this.$store.state.enManagers && this.$store.state.enManagers.length > 0) {
                turnOptions.push(this.$store.state.enManagers[0]);
            }

            if (this.verb.turnId == null && assignee) {
                this.verb.turnId = assignee.userId;
            }
            this.currentTurn = this.verb.turnId
        }

        if (currentStage === 8) {
            let user = this.getUser('payne');
            turnOptions.push(user);
            turnOptions.push(this.$store.state.stageGroups.stage8group);
            this.currentTurn = this.verb.turnId;
        }

        if (currentStage === 9) {
            let user = this.getUser('tanaka');
            turnOptions.push(user);
            turnOptions.push(this.$store.state.stageGroups.stage9group);
            this.currentTurn = this.verb.turnId;
        }

        return turnOptions;
    }

    getUser(username: string): User {
        return this.$store.state.users.find(x => x.username === username);
    }

    @Watch('currentTurn')
    onCurrentTurnChanged() {
        this.$emit('change', this.currentTurn);
    }

    constructor() {
        super();
        this.currentTurn = '';
    }

    created() {
    }
}

Vue.component('turn-selector', TurnSelector)