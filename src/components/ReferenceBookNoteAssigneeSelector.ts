import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'

@Component({
    name: 'reference-book-note-assignee-selector',
    components: {
    }
})
export default class ReferenceBookNoteAssigneeSelector extends Vue {
    public get $store() { return store; }

    @Prop()
    verb: Object;
    @Prop()
    managers: any;

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }
}

Vue.component('reference-book-note-assignee-selector', ReferenceBookNoteAssigneeSelector)