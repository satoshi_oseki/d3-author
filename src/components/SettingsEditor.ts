﻿import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'
import Multiselect from 'vue-multiselect'

import { Message } from './../models/Message'
import MessageView from './MessageView.vue'
import StageSelector from "./StageSelector.vue";
import { Verb } from './../models/Verb'
import store from '../store'

import * as _ from 'lodash'
import axios, { AxiosResponse } from 'axios'
import { VerbLink } from "../models/VerbLink";
import { User } from "../models/User";
import { FinalCheckList } from './../models/FinalCheckList';

@Component({
    name: 'settings-editor',
    components: {
        Multiselect,
        MessageView,
        StageSelector
    }
})
export default class SettingsEditor extends Vue {
    selected: string;
    msoptions: Verb[];
    links1: VerbLink[];
    links2: VerbLink[];
    isVerbsLoading: boolean;
    newMessage: Message;

    public get $store() { return store; }

    public get fsd() {
        return _.get(this, '$store.state.finalFullSentenceDefinition.value');
    }

    public get stages(): number[] {
        let stages: number[] = [];
        const maxStage = this.verb.stage === 9 ? 9 : this.verb.stage + 1;
        for (let i = 1; i <= maxStage; i++) {
            stages.push(i);
        }

        return stages;
    }

    @Prop()
    verb: Verb;
    @Prop()
    jpMembers: User[];
    @Prop()
    enMembers: User[];
    @Prop()
    jpManagers: User[];
    @Prop()
    enManagers: User[];

    constructor() {
        super();

        this.selected = '';
        this.msoptions = [];
        this.links1 = [];
        this.links2 = [];
        this.isVerbsLoading = false;
    }

    created() {
        this.$store.dispatch('loadFinalFullSentenceDefinition', { verbId: this.verb.id })
        if (!this.verb.messages) {
            this.verb.messages = [] as Message[];
        }
        this.verb.links1.forEach(x => {
            axios.get(`/api/verbs?id=${x}`).then((response) => {
                var verb = response.data;
                if (verb) {
                    this.links1.push(verb);
                }
            });
        });
        this.verb.links2.forEach(x => {
            axios.get(`/api/verbs?id=${x}`).then((response) => {
                var verb = response.data;
                if (verb) {
                    this.links2.push(verb);
                }
            });
        });
    }

    updateEntry(verb: Verb) {
        this.save();
    }

    private save(): void {
        this.resetFinalCheckList();
        this.verb.stage = this.$store.state.currentStage;
        this.verb.links1String = this.links1.map(x => x.id).join(',');
        this.verb.links2String = this.links2.map(x => x.id).join(',');
        this.$store.dispatch('updateEntry', { verb: this.verb });
    }

    resetFinalCheckList(): void {
        if (this.verb.stage == 10 && this.$store.state.currentStage < 10) {
            this.$store.state.finalCheckList = {} as FinalCheckList; // Reset check list
            this.$store.dispatch('updateFinalCheckList', { verbId: this.verb.id });
        }
    }

    asyncFindVerb(query) {
        if (!query) {
            this.msoptions = [];
            return;
        }
        this.isVerbsLoading = true;
        axios.get(`/api/verbs/${query}`).then((response) => {
            var verbs = response.data;
            this.msoptions = verbs;
            this.isVerbsLoading = false;
        });
    }

    customLabel(option: Verb) {
        return `${option.value} [${option.structure}] ${option.representativeExample}`;
    }
}

Vue.component('settings-editor', SettingsEditor)