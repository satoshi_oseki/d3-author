import Vue from 'vue';
import { Component, Prop, Watch } from 'vue-property-decorator'

import { Example } from './../models/Example';
import MessageView from './MessageView.vue'
import StageSelector from './StageSelector.vue';
import store from '../store'
import { Verb } from '../models/Verb'

import * as _ from 'lodash'
import { User } from '../models/User';
import TurnSelector from './TurnSelector.vue';
import { FullSentenceDefinition } from '../models/FullSentenceDefinition';
import { D3State } from '../models/D3State';
import { Store } from 'vuex';

@Component({
    name: 'summary-view',
    components: {
        MessageView,
        StageSelector,
        TurnSelector
    }
})
export default class SummaryView extends Vue {
    jpExamples: Example[];
    enExamples: Example[];
    fsdRef: string;
    latestFsd: FullSentenceDefinition;
    currentTurn: string;
    turnOptions: User[] = [];
    localFsdValue: string;

    public get $store() { return store; }

    get verbId(): string {
        return this.verb.id;
    }

    get stage(): number {
        return this.verb ? +this.verb.stage : 0;
    }

    get fsd(): FullSentenceDefinition {
        if (this.stage == 7) { // FSD shows the best choice in stage 7
            return _.find(this.$store.state.candidates.list, 'selected');
        }

        let fsd: any = this.$store.state.latestFullSentenceDefinition;
        if (fsd) {
            fsd.stage = this.stage;
        }

        return fsd;
    }

    get JpExamples(): Example[] {
        return this.getSelectedExamples('jp');
    }

    get EnExamples(): Example[] {
        return this.getSelectedExamples('en');
    }

    @Prop()
    verb: Verb;
    @Prop()
    index: number;
    @Prop()
    jpMembers: any[];
    @Prop()
    enMembers: any[];
    @Prop()
    jpManagers: any[];
    @Prop()
    enManagers: any[];

    @Watch('fsd')
    onFsdRefChanged() {
        this.localFsdValue = this.fsd.value;
    }

    @Watch('currentTurn')
    onCurrentTurnChanged() {
        this.verb.turnId = this.currentTurn;
    }

    constructor() {
        super();
        this.jpExamples = [];
        this.enExamples = [];
        this.fsdRef = this.$store.state.latestFullSentenceDefinition ? this.$store.state.latestFullSentenceDefinition.verbId : '';
        this.latestFsd = JSON.parse(JSON.stringify(this.$store.state.latestFullSentenceDefinition));
        this.currentTurn = null;
        this.localFsdValue = '';
    }

    created() {console.log('Summary View CREATED')
        if (this.stage !== 3 && this.stage !== 7) {
            this.loadExamples(this.index, this.verbId, this.stage);
        }
        this.$store.dispatch('loadFullSentenceDefinition', { verbId: this.verbId, stage: this.stage });
        if (this.stage === 8) {
            this.loadSuggestions(this.verbId);
        }
    }

    loadExamples(index: number, verbId: string, stage: number): void { // required only for stage where there are no example views
        for (let lang = 1; lang <= 2; lang++) {
            for (let i = 0; i < 4; i++) {
                this.$store.dispatch('loadExamples', {
                    index,
                    lang,
                    localId: i,
                    verbId,
                    stage: stage >= 9 ? 8 : this.verb.stage
                });
            }
        }
    }

    loadSuggestions(verbId: string) {
        if (this.stage !== 8) {
            return;
        }

        this.$store.dispatch('loadSuggestions', { verbId: this.verbId });
    }

    // In stage 4 and 5, the English translations can be entered in the Summary tab.
    // They are marked as 'selected'.
    getSelectedExamples(language: 'jp' | 'en'): Example[] { // 'jp' | 'en'
        let examples = [];
        let langId: number = language === 'jp' ? 1 : 2;

        for (let i = 0; i < 4; i++) {
            const exampleList = _.get(this.$store.state, `${language}Examples[${this.index}][${i}].list`);
            // const exampleList = language === 'jp'
            //     ? this.$store.state.jpExamples[this.index][i].list
            //     : this.$store.state.enExamples[this.index][i].list;
            const selected = _.filter(exampleList, 'selected');
            if (selected && selected.length > 0) {
                examples.push(selected[0]);
            } else {
                const localExample = language === 'jp' ? this.jpExamples[i] : this.enExamples[i];
                let example = this.verb.stage > 5
                    ? null
                    : {
                        language: langId,
                        localId: i,
                        status: 0,
                        selected: true,
                        value: localExample ? localExample.value : '',
                        stage: this.verb.stage,
                        verbId: this.verb.id
                    };
                examples.push(example);
            }
        }

        return examples;
    }

    onSaveButtonClick(): void {
        this.save();
    }

    private save(): void {
        this.updateEntry(this.verb);
        this.updateFullSentenceDefinition();
        this.updateExamples(this.verb.stage);
        this.updateSuggestions();
    }

    updateEntry(verb: Verb): void {
        this.verb.stage = this.$store.state.currentStage;
        this.$store.dispatch('updateEntry', { verb });
    }

    updateFullSentenceDefinition(): void {
        if (this.stage <= 3 || this.stage >= 9) {
            return;
        }
        let clonedFsd: any = Object.assign({}, this.fsd);
        clonedFsd.stage = this.stage;
        clonedFsd.value = this.localFsdValue; // bound to textbox v-model
        this.$store.dispatch('updateFullSentenceDefinition', { fsd: clonedFsd });
    }

    updateExamples(stage: number): void {
        if (stage >= 9) { // stage 8 is the last stage that examples can be saved
            return;
        }
        saveExamples(this.$store, this.JpExamples, this.index);
        saveExamples(this.$store, this.EnExamples, this.index);

        function saveExamples(store: Store<D3State>, examples: Example[], index: number) {
            _.each(examples, (ex: Example, localId: number) => {
                ex.stage = stage === 6 ? 7 : stage; // set current stage, skip stage 6 with examples
                store.dispatch('saveExample', { index, ex, localId });
            });
        }
    }

    updateSuggestions(): void {
        if (this.stage !== 8) {
            return;
        }

        this.$store.dispatch('updateSuggestions', { verbId: this.verbId });
    }
}

Vue.component('summary-view', SummaryView)