import Vue from "vue";
import Component from "vue-class-component";

import store from '../store'
import router from '../router'

@Component({
    name: 'Login'
})
export default class Login extends Vue {
    public formUsername: string;
    public formPassword: string;
    public formError: string;
    public authenticating: boolean;

    public get $store() { return store; }

    constructor() {
        super();

        this.formUsername = '';
        this.formPassword = '';
        this.formError = '';
        this.authenticating = false;
    }

    login() {
        this.authenticating = true;
        this.$store.dispatch('login', {
            username: this.formUsername,
            password: this.formPassword
        }).then(res => {
            this.formUsername = ''
            this.formPassword = ''
            this.formError = null;
            this.authenticating = false;

            router.push('/');
        }).catch((e) => {
            this.formError = e.message;
            this.authenticating = false;
        });
    }
}

Vue.component('Login', Login)