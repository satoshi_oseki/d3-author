import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import router from '../router'
import store from '../store'

@Component({
    name: 'header-bar',
    components: {
    }
})
export default class HeaderBar extends Vue {
    public get $store() { return store; }

    logout() {
        this.$store.dispatch('logout');
        router.push('/Login');
    }
}

Vue.component('header-bar', HeaderBar)