import Vue from "vue";
import Component from "vue-class-component";

import store from '../store'
import router from '../router'

import $ from 'jquery'
import axios, { AxiosResponse } from 'axios'

@Component({
    name: 'UploadExcelFile'
})
export default class UploadExcelFile extends Vue {
    public formUsername: string;
    public formPassword: string;
    public formError: string;
    public authenticating: boolean;

    public get $store() { return store; }

    constructor() {
        super();

        this.formUsername = '';
        this.formPassword = '';
        this.formError = '';
        this.authenticating = false;
    }

    uploadExcelFile(): void {
        var data = new FormData();
        var files = $("#files").get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                data.append('files', files[i]);
            }
            //data.append('UploadedImage', files[0]);
        }

        const token = localStorage.getItem('d3');

        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: 'POST',
            url: `/api/UploadFile?token=${token}`,
            //url: `http://localhost:3000/conversion/uploadExcelFile`,
            contentType: false,
            processData: false,
            data: data
        });

        ajaxRequest.done(function (xhr, textStatus) {
            // Do other operation
            window.location.href = '/';
        });
    }

    combineExcelFiles() {
        var sentencesFile = $("#exampleSentenceFile").get(0).files[0];
        var mainFile = $("#mainDataFile").get(0).files[0];

        var data = new FormData();
        //var files = $("#files").get(0).files;
        // Add the uploaded image content to the form data collection

        data.append('files', sentencesFile);
        data.append('files', mainFile);
        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: 'POST',
            url: '/api/CombineFiles',
            contentType: false,
            processData: false,
            data: data
        });

        ajaxRequest.done(function (xhr, textStatus) {
            // Do other operation
            window.location.href = `/api/GetDataFile?filePath=${xhr}`
        });
    }

    exportExcelFull() {
        const token = localStorage.getItem('d3');

        window.location.href = '/api/ExportExcelFull?token=${token}`,';
    }

    exportExcel() {
        const token = localStorage.getItem('d3');

        window.location.href = '/api/ExportExcel?token=${token}`,';
        // axios.get('api/export')
        //     .then(response => {

        //     });
    }
}

Vue.component('UploadExcelFile', UploadExcelFile)