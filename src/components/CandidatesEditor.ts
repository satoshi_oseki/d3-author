import Vue from "vue";

import { alert } from 'vue-strap'
import { Component, Prop, Watch } from 'vue-property-decorator'
import ClickConfirm from 'click-confirm'
import AutosizeTextarea from './AutosizeTextarea.vue';

import store from '../store'

import * as _ from 'lodash'
import moment from 'moment'

@Component({
    name: 'candidates-editor',
    components: {
        ClickConfirm,
        alert,
        AutosizeTextarea
    }
})
export default class CandidatesEditor extends Vue {
    candidateList: any;
    editSelectedIndex: number;
    commentsList: any;
    canAddComments: boolean;

    public get $store() { return store }

    get verbId() { return this.verb.id; }

    @Prop()
    type: string;
    @Prop()
    verb: any;
    @Prop()
    stage: number;
    @Prop()
    candidates: Object;
    @Prop()
    comment: any;

    @Watch('candidates')
    onCandidatesChanged(val) {
        if (val) {
            this.candidateList = _.forEach(val.list, (item) => item.stage = this.stage);
            this.editSelectedIndex = val.selectedIndex;
        }
    }

    @Watch('comments')
    onCommentsChanged(val) {
        if (val) {
            this.commentsList = val
        }
    }

    @Watch('candidateList')
    onCandidateListChanged(val) {
        this.canAddComments = this.candidateList && this.candidateList.length > 0 && this.candidateList[this.candidateList.length - 1].created;
    }

    constructor() {
        super();
        this.candidateList = null;
        this.editSelectedIndex = -1;
        this.commentsList = {};
        this.canAddComments = false;
    }

    convertUtcToLocal(value) {
        return moment.utc(value).local().format('YYYY/MM/DD HH:mm');
    }

    noneSelected() { return _.every(this.candidateList, { 'selected': false }); }

    addComment() {
        let numCandidates = this.candidateList.length;
        let lastCandidate = this.candidateList[numCandidates - 1];
        this.$store.dispatch('addComment', { linkId: lastCandidate.id });
    }

    addCandidate() {
        let verbId = this.verbId;
        let stage = this.stage;
        this.$store.dispatch('addCandidate', { verbId, stage });
    }

    saveCandidate(name, candidate, index) {
        this.$store.dispatch('saveCandidate', { name, candidate, index });
    }

    deleteCandidate(verb, type, candidate, index) {
        this.$store.dispatch('deleteCandidate', { verb, type, candidate, index });
    }

    saveComment(comment, index) {
        this.$store.dispatch('saveComment', { comment, index });
    }

    selectCandidate(verb, type, candidateList, selectedIndex) {
        this.$store.dispatch('selectCandidate', { verb, type, candidateList, selectedIndex });
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }

    deleteComment(comment, index) {
        this.$store.dispatch('deleteComment', { comment, index });
    }
}

Vue.component('candidates-editor', CandidatesEditor)