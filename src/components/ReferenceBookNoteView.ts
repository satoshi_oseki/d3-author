﻿import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import ExampleMapView from './ExampleMapView.vue'
import CandidatesEditor from './CandidatesEditor.vue'
import store from '../store'

@Component({
    name: 'reference-book-note-view',
    components: {
        ExampleMapView, CandidatesEditor
    }
})
export default class ReferenceBookNoteView extends Vue {
    candidatesRef: any;

    public get $store() { return store; }

    get verbId() { return this.verb.id; }

    get stage() { return this.verb.stage; }

    @Prop()
    verb: any;

    constructor() {
        super();
        this.candidatesRef = this.$store.state.candidates
    }

    created() {
        this.$store.dispatch('loadReferenceBookNotes', { verbId: this.verbId, stage: this.verb.stage })
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }
}

Vue.component('reference-book-note-view', ReferenceBookNoteView)