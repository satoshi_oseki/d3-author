import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'

import * as _ from 'lodash'
import { Verb } from "../models/Verb";
import { Example } from "../models/Example";

@Component({
    name: 'entry-editor',
    components: {
    }
})
export default class EntryEditor extends Vue {
    keepExampleSrc: boolean[];
    keepExampleDst: boolean[];

    public get $store() { return store }

    @Prop()
    verb: Verb;
    @Prop()
    index: number;
    @Prop()
    jpExamples: Example[];

    constructor() {
        super();

        this.keepExampleSrc = [true, true, true, true];
        this.keepExampleDst = [false, false, false, false];
    }

    created() {
        this.loadJpExamples(this.verb.id, 1);
    }

    loadJpExamples(verbId: string, stage: number): void { // required only for stage where there are no example views
        for (let i = 0; i < 4; i++) {
            this.$store.dispatch('loadExamples', {
                index: this.index,
                lang: 1, // Jp
                localId: i,
                verbId: verbId,
                stage: stage
            });
        }
    }

    getExampleText(index: number): string {
        let example = this.getExample(index);

        return example ? example.value : '';
    }

    getExample(index: number): Example {
        return _.get(this, `jpExamples[${this.index}][${index}].list`) ? _.find(this.jpExamples[this.index][index].list, (ex) => ex.selected) : null;
    }

    getExamples(): Example[] {
        let examples = [] as Example[];

        for (let i = 0; i < 4; i++) {
            examples.push(this.getExample(i));
        }

        return examples;
    }

    deleteEntry(verb, index): void {
        this.$store.dispatch('deleteEntry', { verb, index });
    }

    cloneEntry(verb, index): void {
        this.$store.dispatch('cloneEntry', {
            verb,
            index,
            examples: this.getExamples(),
            keepExampleSrc: this.keepExampleSrc,
            keepExampleDst: this.keepExampleDst
        });
    }
}

Vue.component('entry-editor', EntryEditor)
