import Vue from 'vue';

import { alert } from 'vue-strap'
import { Component, Prop, Watch } from 'vue-property-decorator'
import ClickConfirm from 'click-confirm'

import store from '../store'

import * as _ from 'lodash'
import moment from 'moment'
import AutosizeTextarea from './AutosizeTextarea.vue';

@Component({
    name: 'sentence-editor',
    components: {
        ClickConfirm,
        alert,
        AutosizeTextarea
    }
})
export default class SentenceEditor extends Vue {
    exampleList: any[];
    editSelectedIndex: number
    commentsList: any;
    canAddComments: boolean;

    public get $store() { return store; }

    @Prop()
    index: number;
    @Prop()
    lang: number;
    @Prop()
    verbId: string;
    @Prop()
    stage: number;
    @Prop()
    localId: number;
    @Prop()
    examples: any;
    @Prop()
    comments: any;

    @Watch('examples')
    onExamplesChanged(val) {
        if (val) {
            this.exampleList = _.forEach(val.list, (item) => item.stage = this.stage);
            this.editSelectedIndex = val.selectedIndex;
        }
    }

    @Watch('comments')
    onCommentsChanged(val) {
        if (val) {
            this.commentsList = val;
        }
    }

    @Watch('exampleList')
    onExampleListChanged(val) {
        this.canAddComments =
            this.exampleList &&
            this.exampleList.length > 0 &&
            this.exampleList[this.exampleList.length - 1].created;
    }

    constructor() {
        super();
        this.exampleList = null;
        this.editSelectedIndex = -1;//this.examples.selectedIndex
        this.commentsList = {};
        this.canAddComments = false;
    }

    convertUtcToLocal(value) {
        return moment.utc(value).local().format('YYYY/MM/DD HH:mm');
    }

    noneSelected() { return _.every(this.exampleList, { 'selected': false }); }

    addComment() {
        let numExamples = this.exampleList.length;
        let lastExample = this.exampleList[numExamples - 1];
        this.$store.dispatch('addComment', { linkId: lastExample.id });
    }

    addExample() {
        const index = this.index;
        let lang = this.lang;
        let verbId = this.verbId;
        let stage = this.stage;
        let localId = this.localId;
        this.$store.dispatch('addExample', { index, lang, verbId, stage, localId });
    }

    saveExample(ex, index) {
        this.$store.dispatch('saveExample', { index: this.index, ex, localId: index });
    }

    deleteExample(example, index) {
        this.$store.dispatch('deleteExample', { index: this.index, example, localId: index });
    }

    saveComment(comment, index) {
        this.$store.dispatch('saveComment', { comment, index });
    }

    saveBestChoice(examples, selectedIndex) {
        this.$store.dispatch('saveBestChoice', { index: this.index, examples, selectedIndex });
    }

    updateEntry(verb) {
        this.$store.dispatch('updateEntry', { verb });
    }

    deleteComment(comment, index) {
        this.$store.dispatch('deleteComment', { comment, index });
    }
}

Vue.component('sentence-editor', SentenceEditor)