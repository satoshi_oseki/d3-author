import Vue from 'vue';
import { Component, Prop, Watch } from 'vue-property-decorator'

import store from '../store'
import { Verb } from '../models/Verb'

import * as _ from 'lodash'

@Component({
    name: 'stage-selector'
})
export default class StageSelector extends Vue {
    public get $store() { return store; }

    get stage(): number {
        return this.verb ? +this.verb.stage : 0;
    }

    public get availableStages(): number[] {
        let stages: number[] = [];
        const maxStage = this.isDevelopment || this.verb.stage === 10 ? 10 : this.verb.stage + 1;
        for (let i = 1; i <= maxStage; i++) {
            stages.push(i);
        }

        return stages;
    }

    private get isDevelopment(): boolean {
        return window.location.hostname === 'localhost';
    }

    @Prop()
    verb: Verb;

    constructor() {
        super();
        this.$store.dispatch('setCurrentStage', { stage: this.verb.stage })
    }

    created() {
    }
}

Vue.component('stage-selector', StageSelector)