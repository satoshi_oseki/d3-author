import Vue from "vue";
import Component from "vue-class-component";

import $ from 'jquery'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

@Component({
  name: "App",
})
export default class App extends Vue {
}