﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using d3_author.Models;
using d3_author.Models.Optons;
using d3_author.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using OfficeOpenXml;

namespace d3_author.Controllers {
  public class UploadController : Controller {
    private readonly IUserService _userService;
    private IEnumerable<User> _users;

    private readonly AppSettings _appSettings;

    private static MongoClient _client;

    private static Dictionary<string, bool> _exporting = new Dictionary<string, bool>(); // <token, isExporting>

    private int TurnColumnIndex { get { return GetEnExampleColumnIndex(STAGE.STAGE_8) + 4; }}
    private int IdColumnIndex { get { return TurnColumnIndex + 1; }}

    public UploadController(IUserService userService, IOptions<AppSettings> optionsAccessor) {
      var conventionPack = new  ConventionPack {new CamelCaseElementNameConvention()};
      ConventionRegistry.Register("camelCase", conventionPack, t => true);      
      _userService = userService;
      _appSettings = optionsAccessor.Value;
      var mongoDbConnection = _appSettings.MongoDbConnection; //configuration.GetSection("AppSettings").GetValue<string>("MongoDbConnection");
      _client = new MongoClient(mongoDbConnection);
    }

    [Route("api/UploadFile")]
    [HttpPost]
    public async Task<IActionResult> UploadFile(IList<IFormFile> files, [FromQuery] string token) {
      _users = await _userService.GetAllUsersAsync(token);

      long size = files.Sum(f => f.Length);

      // full path to file in temp location
      var filePath = Path.GetTempFileName();

      foreach (var formFile in files) {
        if (formFile.Length > 0) {
          using(var stream = new FileStream(filePath, FileMode.Create)) {
            await formFile.CopyToAsync(stream);
          }
        }
      }

      DoConvert(filePath);

      // process uploaded files
      // Don't rely on or trust the FileName property without validation.

      return Ok(new {
        count = files.Count, size, filePath
      });
    }

    private void DoConvert(string fileFullPath) {
      FileInfo file = new FileInfo(fileFullPath);
      try {
        using(ExcelPackage package = new ExcelPackage(file)) {
          StringBuilder sb = new StringBuilder();
          ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
          int rowCount = worksheet.Dimension.Rows;
          int colCount = worksheet.Dimension.Columns;

          DataTable dt = new DataTable(colCount, rowCount);
          for (int row = 1; row <= rowCount; row++) {
            for (int col = 1; col <= colCount; col++) {
              if (row > 1) {
                dt.SetData(col - 1, row - 1, worksheet.Cells[row, col]?.Value?.ToString());
              }
            }
            sb.Append(Environment.NewLine);
          }

          List<Verb> verbs;
          List<FullSentenceDefinition> fsd;
          List<Example> examples;

          ConvertToModel(dt, colCount, out verbs, out fsd, out examples);

          var dddDb = _client.GetDatabase("ddd");
          dddDb.DropCollection("verbs");
          dddDb.DropCollection("full-sentence-definitions");
          dddDb.DropCollection("examples");

          if (verbs.Count > 0) {
            var verbCollection = dddDb.GetCollection<Verb>("verbs");
            verbCollection.InsertMany(verbs);
          }

          if (fsd.Count > 0) {
            var fsdCollection = dddDb.GetCollection<FullSentenceDefinition>("full-sentence-definitions");
            fsdCollection.InsertMany(fsd);
          }

          if (examples.Count > 0) {
            var exampleCollection = dddDb.GetCollection<Example>("examples");
            exampleCollection.InsertMany(examples);
          }

          // using (var db = _context)
          // {
          //     db.Verbs.RemoveRange(db.Verbs);
          //     db.Verbs.AddRange(verbs);

          //     db.FullSentenceDefinitions.RemoveRange(db.FullSentenceDefinitions);
          //     db.FullSentenceDefinitions.AddRange(fsd);

          //     db.Examples.RemoveRange(db.Examples);
          //     db.Examples.AddRange(examples);

          //     db.SaveChanges();
          // }
        }
      }
      catch (Exception ex) {
        throw new Exception("Some error occured while importing." + ex.Message);
      }
    }
    private void ConvertToModel
    (
      DataTable dt,
      int numCols,
      out List<Verb> verbs,
      out List<FullSentenceDefinition> fsd,
      out List<Example> examples
    )
    {
      verbs = new List<Verb>();
      fsd = new List<FullSentenceDefinition>();
      examples = new List<Example>();

      foreach (DataRow row in dt.Rows)
      {
        //var numItems = row.ItemArray.Count;
        if (row?.ItemArray[2] == null || row?.ItemArray[2] == string.Empty)
        {
          continue;
        }

        var verbId = IdColumnIndex < numCols
          ? row?.ItemArray[IdColumnIndex] as string
          : Guid.NewGuid().ToString();
        var stage = string.IsNullOrEmpty(row.ItemArray[4])
          ? STAGE.STAGE_1
          : (STAGE) Convert.ToInt32(row.ItemArray[4] as string);
        var turnId = TurnColumnIndex < numCols
          ? row?.ItemArray[TurnColumnIndex] as string
          : "";
        var verb = new Verb
        {
          Id = verbId,
          IdForLinks = !string.IsNullOrEmpty(row?.ItemArray[1]) // 0-base, initial upload
          ?
          row?.ItemArray[1] as string :
          row?.ItemArray[0] as string,
          Value = row?.ItemArray[2] as string,
          LocalIndex = row?.ItemArray[3] == string.Empty ?
          0 : (int) Convert.ToInt32(row?.ItemArray[3] as string),
          EnglishIndex = row?.ItemArray[9] as string,
          Structure = row?.ItemArray[10] as string,
          NoteForReferenceBook = row?.ItemArray[11] as string,
          RepresentativeExample = row?.ItemArray[12] as string,
          NoteForExampleAuthor = row?.ItemArray[13] as string,
          GeneralNotes = row?.ItemArray[14] as string,
          //Examples = new List<Tuple<Example, Example>>[(int)STAGE.STAGE_8],
          JpAssigneeId = GetUserId(_users, row?.ItemArray[15] as string),
          JpCheckerId = GetUserId(_users, row?.ItemArray[16] as string),
          EnAssigneeId = GetUserId(_users, row?.ItemArray[17] as string),
          EnCheckerId = GetUserId(_users, row?.ItemArray[18] as string),
          // FullSentenceDefinition = row?.ItemArray[23] as string,
          Stage = stage,
          Selected = false,
          Links1String = "",
          Links2String = "",
          Links1Raw = row?.ItemArray[5] as string,
          Links2Raw = row?.ItemArray[6] as string
        };
        SetTurnId(verb, turnId);
        // Attach messages to this verb
        var dddDb = _client.GetDatabase("ddd");
        var messageCollection = dddDb.GetCollection<Message>("messages");
        var cursor = messageCollection.Find<Message>(m => m.VerbId == verbId);
        if (cursor != null && cursor.Any())
        {
          verb.Messages = cursor?.ToList();
        }
 
        verbs.Add(verb);

        verb.FullSentenceDefinition = AddFullSentenceDefinition(row, verb, fsd);
        AddJpExamples(row, verb, examples);
        AddEnExamples(row, verb, examples);
      }

      AddLinks(verbs);
    }

    private void SetTurnId(Verb verb, string usernameInFile)
    {
      if (verb.Stage == STAGE.STAGE_2 || verb.Stage == STAGE.STAGE_6)
      {
        return;
      }
      if (string.IsNullOrEmpty(usernameInFile))
      {
        if (verb.Stage == STAGE.STAGE_3)
        {
          verb.TurnId = verb.JpAssigneeId;
        }
        else if (verb.Stage == STAGE.STAGE_7)
        {
          verb.TurnId = verb.EnCheckerId;
        }
        else
        {
          verb.TurnId = _userService.GetDefaultUserId(verb.Stage);
        }
      }
      else
      {
        verb.TurnId = GetUserId(_users, usernameInFile);
      }
    }
    private int GetFsdColumnIndex(STAGE stage)
    {
      var jpColumn = GetJpExampleColumnIndex(stage);
      return jpColumn > 0 ? jpColumn - 1 : -1;
    }

    private string AddFullSentenceDefinition(DataRow row, Verb verb, IList<FullSentenceDefinition> list)
    {
      string lastFsd = null;
      for (var stage = STAGE.STAGE_4; stage <= verb.Stage; stage++)
      {
        var column = GetFsdColumnIndex(stage);
        if (column < 0)
        {
          continue;
        }
        var fsd = row?.ItemArray[column];
        if (!string.IsNullOrEmpty(fsd))
        {
          lastFsd = fsd;
          list.Add(
            new FullSentenceDefinition
            {
              Id = Guid.NewGuid().ToString(),
              VerbId = verb.Id,
              Value = fsd,
              Created = GetUtcEpoch(),
              Updated = GetUtcEpoch(),
              Stage = stage,
              Selected = true
            }
          );
        }
      }

      return lastFsd;
    }

    private int GetJpExampleColumnIndex(STAGE stage)
    {
      // stage 3: 19
      // stage 4: 24 (4 examples + 1 fsd)
      // stage 5: 33 (4 + 4 examples + 1 fsd)
      // stage 6: none
      // stage 7: 42
      // stage 8: 51
      switch (stage)
      {
        case STAGE.STAGE_3: return 19;
        case STAGE.STAGE_4: return 24;
        case STAGE.STAGE_5: return 33;
        case STAGE.STAGE_7: return 42;
        case STAGE.STAGE_8: return 51;
        default: return -1;
      }
    }

    private int GetEnExampleColumnIndex(STAGE stage)
    {
      var jpColumn = GetJpExampleColumnIndex(stage);
      return jpColumn > 0 ? jpColumn + 4 : -1;
    }

    private void AddJpExamples(DataRow row, Verb verb, IList<Example> list)
    {
      for (var stage = STAGE.STAGE_3; stage <= verb.Stage; stage++)
      {
        var startColumn = GetJpExampleColumnIndex(stage);
        if (startColumn < 0)
        {
          continue;
        }
        for (var i = 0; i < 4; i++)
        {
          if (!string.IsNullOrEmpty(row?.ItemArray?[startColumn + i])) {
            list.Add(
              new Example
              {
                Id = Guid.NewGuid().ToString(),
                LocalId = i,
                VerbId = verb.Id,
                Value = row?.ItemArray?[startColumn + i] as string,
                Created = GetUtcEpoch(),
                Updated = GetUtcEpoch(),
                Stage = stage,
                Language = LANG.JP,
                Selected = true
              }
            );
          }
        }
      }
    }

    private void AddEnExamples(DataRow row, Verb verb, IList<Example> list)
    {
      for (var stage = STAGE.STAGE_4; stage <= verb.Stage; stage++)
      {
        var startColumn = GetEnExampleColumnIndex(stage);
        if (startColumn < 0)
        {
          continue;
        }
        for (var i = 0; i < 4; i++)
        {
          if (!string.IsNullOrEmpty(row?.ItemArray?[startColumn + i])) {
            list.Add(
              new Example
              {
                Id = Guid.NewGuid().ToString(),
                LocalId = i,
                VerbId = verb.Id,
                Value = row?.ItemArray?[startColumn + i] as string,
                Created = GetUtcEpoch(),
                Updated = GetUtcEpoch(),
                Stage = stage,
                Language = LANG.EN,
                Selected = true
              }
            );
          }
        }
      }
    }

    private string GetUserId(IEnumerable<User> users, string username) {
      return users.Where(x => x.Username == username).Select(x => x.UserId).FirstOrDefault();
    }

    private static long GetUtcEpoch() {
      TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);

      return (long) t.TotalMilliseconds;
    }

    private void AddLinks(IEnumerable<Verb> verbs) {
      foreach (var verb in verbs) {
        var links1 = new List<string>();
        var links2 = new List<string>();
        if (!string.IsNullOrEmpty(verb.Links1Raw)) {
          var linkIds = verb.Links1Raw.Split(',');
          foreach (var linkId in linkIds) {
            var verbId = GetVerbId(verbs, linkId.Trim());
            links1.Add(verbId);
          }
          verb.Links1 = links1;
        }

        if (!string.IsNullOrEmpty(verb.Links2Raw)) {
          var linkIds = verb.Links2Raw.Split(',');
          foreach (var linkId in linkIds) {
            var verbId = GetVerbId(verbs, linkId.Trim());
            links2.Add(verbId);
          }
          verb.Links2 = links2;
        }
      }
    }

    private string GetVerbId(IEnumerable<Verb> verbs, string linkId) {
      foreach (var verb in verbs) {
        if (verb.IdForLinks == linkId) {
          return verb.Id;
        }
      }

      return null;
    }
  }
}