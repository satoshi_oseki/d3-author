﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using d3_author.Models;
using d3_author.Models.Optons;
using d3_author.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using OfficeOpenXml;

namespace d3_author.Controllers {
  public class ExportController : Controller {
    private readonly IUserService _userService;
    private readonly AppSettings _appSettings;

    private static MongoClient _client;

    private static Dictionary<string, bool> _exporting = new Dictionary<string, bool> (); // <token, isExporting>

    public ExportController (IUserService userService, IOptions<AppSettings> optionsAccessor) {
      var conventionPack = new  ConventionPack {new CamelCaseElementNameConvention()};
      ConventionRegistry.Register("camelCase", conventionPack, t => true);
      _userService = userService;
      _appSettings = optionsAccessor.Value;
      var mongoDbConnection = _appSettings.MongoDbConnection; //configuration.GetSection("AppSettings").GetValue<string>("MongoDbConnection");
      _client = new MongoClient (mongoDbConnection);
    }

    [HttpGet ("api/isExporting")]
    public IActionResult IsExporting ([FromQuery] string token) {
      var isExporting = _exporting.ContainsKey (token) ? _exporting[token] : false;

      return Ok (isExporting);
    }

    [Route ("api/ExportExcelFull")]
    [HttpGet]
    public FileResult ExportExcelFull ([FromQuery] string token) {
      _exporting.Add (token, true);
      var exportTask = DoExportExcel (token, true);
      exportTask.Wait ();
      _exporting.Remove (token);
      return exportTask.Result;
    }

    [Route ("api/ExportExcel")]
    [HttpGet]
    public FileResult ExportExcel ([FromQuery] string token, bool full = false) {
      _exporting.Add (token, true);
      var exportTask = DoExportExcel (token, false);
      exportTask.Wait ();
      _exporting.Remove (token);
      return exportTask.Result;
    }

    private async Task<FileResult> DoExportExcel ([FromQuery] string token, bool full = false) {
      var memoryStream = new MemoryStream ();
      var outputFile = new ExcelPackage (memoryStream);
      var sheet = outputFile.Workbook.Worksheets.Add ("D3Sheet");
      var db = _client.GetDatabase ("ddd");
      var verbCollection = db.GetCollection<Verb> ("verbs");
      var fsdCollection = db.GetCollection<FullSentenceDefinition> ("full-sentence-definitions");
      var exampleCollection = db.GetCollection<Example> ("examples");
      var notesCollection = db.GetCollection<NoteForReferenceBook> ("note-for-reference-book");
      var verbsCursor = await verbCollection.FindAsync (x => x.Status == STATUS.ACTIVE,
        new FindOptions<Verb> {
          Sort = Builders<Verb>.Sort.Ascending ("Value").Ascending ("LocalIndex")
        });
      var verbs = verbsCursor.ToList ();
      var fsds = fsdCollection.Find (new BsonDocument ()).ToList ();
      var examples = exampleCollection.Find (new BsonDocument ()).ToList ();
      var notes = notesCollection.Find (new BsonDocument ()).ToList ();

      await DoExport (sheet, verbs, examples, fsds, notes, token, full);

      outputFile.Save ();
      memoryStream.Flush ();
      memoryStream.Position = 0;

      var today = DateTime.Now.ToString ("yyyyMMdd");
      var fullSuffix = full ? "-full" : "";
      var exportFileName = $"ddd-export{fullSuffix}-{today}.xlsx";
      return File (memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", exportFileName);
    }

    private string ConvertLinkIdsToExcelIds (IList<Verb> verbs, IEnumerable<string> links) {
      if (links == null || links.Count () == 0) {
        return string.Empty;
      }

      var numVerbs = verbs.Count;
      var ids = new List<string> ();
      foreach (var l in links) {
        for (var i = 0; i < numVerbs; i++) {
          if (verbs[i].Id == l) {
            ids.Add ((i + 1).ToString ());
          }
        }
      }

      return string.Join (",", ids);
    }

    private string FindFinalExample (List<Example> examples, LANG lang, string verbId, int localId /* 0 - 3 */ ) {
      var query = (from b in examples where b.Language == lang && b.VerbId == verbId && b.LocalId == localId orderby b.Stage descending select b);

      return query != null && query.Count () > 0 ? query.ToArray () [0].Value : "";
    }

    private string FindFinalFsd (List<FullSentenceDefinition> fsds, string verbId) {
      var query = (from b in fsds where b.VerbId == verbId orderby b.Stage descending select b);

      return query != null && query.Count () > 0 ? query.ToArray () [0].Value : "";
    }

    private string FindExampleByStage (List<Example> examples, int stage, LANG lang, string verbId, int localId /* 0 - 3 */ ) {
      var query = (from b in examples where b.Status == STATUS.ACTIVE &&
        b.Language == lang &&
        b.VerbId == verbId &&
        b.LocalId == localId &&
        (int) b.Stage == stage select b);

      return query != null && query.Count () > 0 ? query.ToArray () [0].Value : "";
    }

    private string FindFsdByStage (List<FullSentenceDefinition> fsds, int stage, string verbId) {
      var query = (from b in fsds where b.Status == STATUS.ACTIVE &&
        b.VerbId == verbId &&
        (int) b.Stage == stage select b);

      return query != null && query.Count () > 0 ? query.ToArray () [0].Value : "";
    }

    private async Task DoExport (
      ExcelWorksheet worksheet,
      List<Verb> verbs,
      List<Example> examples,
      List<FullSentenceDefinition> fsds,
      List<NoteForReferenceBook> notesForRefBook,
      string token,
      bool full) {
      AddHeader (worksheet, full);
      var row = 2;
      foreach (var verb in verbs) {
        var jpAssignee = await _userService.GetUserById (verb.JpAssigneeId, token);
        var jpChecker = await _userService.GetUserById (verb.JpCheckerId, token);
        var enAssignee = await _userService.GetUserById (verb.EnAssigneeId, token);
        var enChecker = await _userService.GetUserById (verb.EnCheckerId, token);
        worksheet.Cells[row, 1].Value = row - 1;
        worksheet.Cells[row, 3].Value = verb.Value;
        worksheet.Cells[row, 4].Value = verb.LocalIndex;
        worksheet.Cells[row, 5].Value = (int) verb.Stage;
        worksheet.Cells[row, 6].Value = ConvertLinkIdsToExcelIds (verbs, verb.Links1); // LInks
        worksheet.Cells[row, 7].Value = ConvertLinkIdsToExcelIds (verbs, verb.Links2);
        worksheet.Cells[row, 10].Value = verb.EnglishIndex;
        worksheet.Cells[row, 11].Value = verb.Structure;
        worksheet.Cells[row, 12].Value = verb.NoteForReferenceBook;
        worksheet.Cells[row, 13].Value = verb.RepresentativeExample;
        worksheet.Cells[row, 14].Value = verb.NoteForExampleAuthor;
        worksheet.Cells[row, 15].Value = verb.GeneralNotes;
        worksheet.Cells[row, 16].Value = jpAssignee?.Username;
        worksheet.Cells[row, 17].Value = jpChecker?.Username;
        worksheet.Cells[row, 18].Value = enAssignee?.Username;
        worksheet.Cells[row, 19].Value = enChecker?.Username;

        var column = 20;
        if (full) {
          for (var stage = 3; stage <= 8; stage++) {
            if (stage == 6) { continue; }

            if (stage > 3) {
              worksheet.Cells[row, column].Value = FindFsdByStage (fsds, stage, verb.Id);
              column++;
            }

            for (var localId = 0; localId < 4; localId++) {
              worksheet.Cells[row, column].Value = FindExampleByStage (examples, stage, LANG.JP, verb.Id, localId);
              column++;
            }

            if (stage == 3) { continue; }

            for (var localId = 0; localId < 4; localId++) {
              worksheet.Cells[row, column].Value = FindExampleByStage (examples, stage, LANG.EN, verb.Id, localId);
              column++;
            }
          }
          // Turn
          var userInTurn =  await _userService.GetUserById(verb.TurnId, token);
          worksheet.Cells[row, column++].Value =userInTurn?.Username; // export as username
          // ID
          worksheet.Cells[row, column].Value = verb.Id;
        } else {
          worksheet.Cells[row, column++].Value = FindFinalFsd (fsds, verb.Id);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.JP, verb.Id, 0);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.JP, verb.Id, 1);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.JP, verb.Id, 2);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.JP, verb.Id, 3);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.EN, verb.Id, 0);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.EN, verb.Id, 1);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.EN, verb.Id, 2);
          worksheet.Cells[row, column++].Value = FindFinalExample (examples, LANG.EN, verb.Id, 3);
        }

        row++;
      }
    }

    private string[] commonHeaders = new [] {
      "VerbAppEntryでの順番",
      "Excel作業での項目番号",
      "Entry word",
      "partial ordering number",
      "Stage",
      "自他対応グループ",
      "同義異格配列グループ",
      "グループX",
      "グループY",
      "English index",
      "Structure",
      "本の備考欄",
      "Representative examples",
      "提示文作成者への注意事項",
      "使い方メモ(本の備考欄用)",
      "日本語作成者",
      "日本語作成確認者",
      "英語改善者",
      "英語改善確認者"
    };

    private void AddHeader (ExcelWorksheet worksheet, bool full) {
      const int row = 1;
      var headers = new List<string> ();
      headers.AddRange (commonHeaders);
      if (full) {
        headers.AddRange (GetFsdAndExampleHeaders ());
      } else {
        headers.AddRange (GetFsdAndExampleHeadersFinal ());
      }
      headers.Add("Turn");
      headers.Add("Verb ID");
      var column = 1;
      headers.ForEach (x => {
        worksheet.Cells[row, column++].Value = x;
      });
    }

    private string[] GetFsdAndExampleHeaders () {
      var headers = new List<string> ();
      for (var stage = 3; stage < 9; stage++) {
        if (stage == 6) {
          continue;
        }
        if (stage > 3) {
          headers.Add ($"S{stage}-Full sentence definition");
        }
        for (var i = 1; i <= 4; i++) {
          headers.Add ($"S{stage}-提示文#{i}");
        }
        if (stage > 3) {
          for (var i = 1; i <= 4; i++) {
            headers.Add ($"S{stage}-英語#{i}");
          }
        }
      }

      return headers.ToArray ();
    }

    private string[] GetFsdAndExampleHeadersFinal () {
      var headers = new List<string> ();
      headers.Add ($"Full sentence definition");
      for (var i = 1; i <= 4; i++) {
        headers.Add ($"提示文#{i}");
      }
      for (var i = 1; i <= 4; i++) {
        headers.Add ($"英語#{i}");
      }

      return headers.ToArray ();
    }

  }
}