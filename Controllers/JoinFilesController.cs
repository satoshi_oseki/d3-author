﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using d3_author.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using d3_author.Models.Optons;
using Microsoft.Extensions.Options;

namespace d3_author.Controllers
{
  public class JoinFilesController : Controller
  {
    [Route("api/CombineFiles")]
    [HttpPost]
    public async Task<IActionResult> CombineFiles(IList<IFormFile> files)
    {
      var sentenceFilePath = Path.GetTempFileName();
      var mainFilePath = Path.GetTempFileName();
      var outputFilePath = Path.GetTempFileName();

      using (var sentenceStream = new FileStream(sentenceFilePath, FileMode.Create))
      {
        await files[0].CopyToAsync(sentenceStream);
        using(var mainStream = new FileStream(mainFilePath, FileMode.Create))
        {             
          await files[1].CopyToAsync(mainStream);
          var sentenceDataTable = LoadExampleData(sentenceStream);
          EmbedData(sentenceDataTable, mainStream, outputFilePath);
          return Ok(outputFilePath);
        }
      }
    }

    [Route("api/GetDataFile")]
    [HttpGet]
    public FileResult GetDataFile([FromQuery]string filePath)
    {
        var stream = new FileStream(filePath, FileMode.Open);

        return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", "ddd-data.xlsx"); 
    }

    private void EmbedData(DataTable dt, FileStream targetStream, string outputFilePath)
    {
      using(var outputStream = new FileStream(outputFilePath, FileMode.Create))
      {
        //var memoryStream = new MemoryStream();
        var outputFile = new ExcelPackage(targetStream);
        var sheet = outputFile.Workbook.Worksheets[1]; // 1-base

        const int numExamplesPerVerb = 4;
        const int jpExampleColumn = 2; // 0-base
        const int enExampleColumn = 3; // 0-base
        const int fsdColumn = 5; // 0-base
        const int jpBaseColumn = 20; // i-base
        const int enBaseColumn = 29; // 1-base
        const int fsdBaseColumn = 24; // 1-base
        var rowIndex = 1; // first row is header
        var jpExamples = new List<string>();
        var enExamples = new List<string>();
        var fsd = "";
        foreach (DataRow row in dt.Rows)
        {
          if (rowIndex == 1) { rowIndex++; continue; }
          jpExamples.Add(row.ItemArray[jpExampleColumn]);
          enExamples.Add(row.ItemArray[enExampleColumn]);
          if (rowIndex % 4 == 2)
          {
            fsd = row.ItemArray[fsdColumn];
          }
          if (rowIndex % 4 == 1) // flush examples every four rows
          {
            var outputRowIndex = rowIndex / 4 + 1;
            var stage = Convert.ToInt32(sheet.Cells[outputRowIndex, 5].Value.ToString());
            for (var outPutStage = (int)STAGE.STAGE_3; outPutStage <= (int)stage; outPutStage++)
            {
              if (outPutStage == (int)STAGE.STAGE_6
                  || outPutStage == (int)STAGE.STAGE_9)
              {
                  continue;
              }

              for (var col = 0; col < numExamplesPerVerb; col++)
              {
                  sheet.Cells[rowIndex / 4 + 1, GetJpBaseColumn(outPutStage) + col].Value = jpExamples[col];
                  if (outPutStage > 3)
                  {
                      sheet.Cells[rowIndex / 4 + 1, GetEnBaseColumn(outPutStage) + col].Value = enExamples[col];
                  }
              }
              if (outPutStage > 3)
              {
                  sheet.Cells[rowIndex / 4 + 1, GetFsdBaseColumn(outPutStage)].Value = fsd;
              }
            }

            jpExamples.Clear();
            enExamples.Clear();
          }
          rowIndex++;
        }

        // sheet.Cells[1, 1].Value = "Foo"; // セルA1に書き込み
        // sheet.Cells["A2"].Value = "Bar"; // セルA2に書き込み
        // sheet.Cells["B1"].Value = "Baz"; // セルB1に書き込み
        targetStream.Flush();
        targetStream.Position = 0;
        outputFile.SaveAs(outputStream);
      }
    }

    private int GetJpBaseColumn(int stage)
    {
      switch (stage)
      {
        case 3: return 20;
        case 4: return 25;
        case 5: return 34;
        case 7: return 43;
        case 8: return 52;
        default: throw new Exception($"GetJpBaseColumn: invalid stage {stage}");
      }
    }

    private int GetEnBaseColumn(int stage)
    {
      switch (stage)
      {
        case 4: return 29;
        case 5: return 38;
        case 7: return 47;
        case 8: return 56;
        default: throw new Exception($"GetJpBaseColumn: invalid stage {stage}");
      }
    }

    private int GetFsdBaseColumn(int stage)
    {
      switch (stage)
      {
        case 4: return 24;
        case 5: return 33;
        case 7: return 42;
        case 8: return 51;
        default: throw new Exception($"GetJpBaseColumn: invalid stage {stage}");
      }
    }

    private DataTable LoadExampleData(Stream stream)// fileFullPath)
    {
      // FileInfo file = new FileInfo(fileFullPath);
      try
      {
        using (ExcelPackage package = new ExcelPackage(stream))
        {
          ExcelWorksheet worksheet = package.Workbook.Worksheets["Display"];
          int rowCount = worksheet.Dimension.Rows;
          int colCount = worksheet.Dimension.Columns;

          DataTable dt = new DataTable(colCount, rowCount);
          for (int row = 2/* first row is header */; row <= rowCount; row++)
          {
            for (int col = 1; col <= colCount; col++)
            {
              if (row >= 1)
              {
                dt.SetData(col - 1, row - 1, worksheet.Cells[row, col]?.Value?.ToString());
              }
            }
          }

          return dt;
        }
      }
      catch (Exception ex)
      {
          throw new Exception("Some error occured while importing." + ex.Message);
      }          
    }
  }
}
