using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

using d3_author.Models;
using d3_author.Services;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using d3_author.Models.Optons;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using System.Linq.Expressions;

namespace d3_author.Controllers
{
    public class D3Controller : Controller
    {
        private const int NumItemsPerPage = 20;
        private readonly IUserService _userService;
        private readonly AppSettings _appSettings;

        private MongoClient _client;

        public D3Controller(IUserService userService, IOptions<AppSettings> optionsAccessor)
        {
            var conventionPack = new  ConventionPack {new CamelCaseElementNameConvention()};
            ConventionRegistry.Register("camelCase", conventionPack, t => true);
            _userService = userService;
            _appSettings = optionsAccessor.Value;
            var mongoDbConnection = _appSettings.MongoDbConnection;//configuration.GetSection("AppSettings").GetValue<string>("MongoDbConnection");
            _client = new MongoClient(mongoDbConnection);
        }

        [HttpGet, Route("api/numEntries")]
        public IActionResult GetNumberOfEntries()
        {
            try
            {
                return Ok(GetNumberOfVerbs());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        private long GetNumberOfVerbs()
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Verb>("verbs");

            return (collection != null) ? collection.Count(x => x.Status == STATUS.ACTIVE) : (long)0;
        }

        [HttpGet, Route("api/verbs/{page:int}/{numItemsPerPage:int}/{stage:int?}/{userId?}")]
        public async Task<IActionResult> GetAllDocs(int page, int numItemsPerPage, int stage = 0, string userId = null, [FromQuery] string search = "")
        {
            try
            {
                var dddDb = _client.GetDatabase("ddd");
                var verbCollection = dddDb.GetCollection<Verb>("verbs");
                var verbListResponse = new VerbListResponse();
                if (verbCollection == null || verbCollection.Count(x => x.Status == STATUS.ACTIVE) == 0)
                {
                    verbListResponse.NumVerbs = 0;
                    verbListResponse.Verbs = new List<Verb>();
                    return Ok(verbListResponse);
                }
                var verbs = await verbCollection.FindAsync
                    (x => (x.Status == STATUS.ACTIVE &&
                    stage == 0 || (int)x.Stage == stage) &&
                    !string.IsNullOrEmpty(x.Value) &&
                    (userId == null ||
                    //x.Assignees != null && x.Assignees.Any(a => a.UserId == userId) || // only use turn for filtering
                    x.TurnId == userId) &&
                    (search == string.Empty || x.Value.StartsWith(search)),
                    new FindOptions<Verb>
                    {
                        Sort = Builders<Verb>.Sort.Ascending("Value").Ascending("LocalIndex"),
                        Limit = numItemsPerPage,
                        Skip = page * numItemsPerPage
                    });

                var token = Request.Query["token"];
                var verbList = verbs.ToList();
                foreach (var item in verbList)
                {
                    item.Assignees = await GetAssignees(item, token);
                    // item.FullSentenceDefinition = (await GetAllItems<FullSentenceDefinition>(item.Id, (int)STAGE.STAGE_11)).FirstOrDefault()?.Value;
                    // item.NoteForReferenceBook = (await GetAllItems<NoteForReferenceBook>(item.Id, (int)STAGE.STAGE_11)).FirstOrDefault()?.Value;
                }

                verbListResponse.Verbs = verbList;
                verbListResponse.NumVerbs = verbCollection.Count(x =>
                    (x.Status == STATUS.ACTIVE &&
                    stage == 0 || (int)x.Stage == stage) &&
                    !string.IsNullOrEmpty(x.Value) &&
                    (userId == null ||
                    //x.Assignees != null && x.Assignees.Any(a => a.UserId == userId) || // only use turn for filtering
                    x.TurnId == userId) &&
                    (search == string.Empty || x.Value.StartsWith(search))
                );

                return Ok(verbListResponse);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Forbidden")
                {
                    return StatusCode((int)HttpStatusCode.Forbidden, ex);
                }
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        private async Task<List<User>> GetAssignees(Verb verb, string token)
        {
            var ringe = await _userService.GetUser("hayashishita", token);
            var ueyama = await _userService.GetUser("ueyama", token);
            var tanaka = await _userService.GetUser("tanaka", token);
            var rachel = await _userService.GetUser("payne", token);

            switch (verb.Stage)
            {
                case STAGE.STAGE_1:
                    return new List<User>{ ringe, ueyama };
                case STAGE.STAGE_2:
                case STAGE.STAGE_4:
                case STAGE.STAGE_6:
                case STAGE.STAGE_8:
                    return new List<User>{ ringe };
                case STAGE.STAGE_3:
                    return new List<User>{ await _userService.GetUserById(verb.JpAssigneeId, token) ?? tanaka };
                case STAGE.STAGE_5:
                    return new List<User>{ ringe, ueyama, tanaka };
                case STAGE.STAGE_7:
                    return new List<User>{ await _userService.GetUserById(verb.EnAssigneeId, token) ?? ringe };
                case STAGE.STAGE_9:
                    return new List<User>{ await _userService.GetUserById(verb.RefAssigneeId, token) ?? ringe };
                case STAGE.STAGE_10:
                    return new List<User>{};
                default:
                    throw new Exception("Invalid stage");
            }
        }

        [HttpGet]
        [Route("api/examples/{lang}/verbId/{verbId}/localId/{localId}/stage/{stage}")]
        public async Task<IActionResult> GetExamples(int lang, string verbId, int localId, int stage)
        {
            // if (stage < 3)
            // {
            //     stage = 3; // Copying an entry in Stage 1 needs to get Japanese examples which are stored in Stage 3. 
            // }

            try
            {
                var dddDb = _client.GetDatabase("ddd");
                var exampleCollection = dddDb.GetCollection<Example>("examples");
                var examples = await exampleCollection.FindAsync(
                    x => (
                        x.Status == STATUS.ACTIVE &&
                        x.Language == (LANG)lang &&
                        x.VerbId == verbId &&
                        x.LocalId == localId &&
                        (int)x.Stage == stage
                        ), new FindOptions<Example> { Sort = Builders<Example>.Sort.Descending("Stage") });

                return Ok(examples.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/examples/{lang}/verbId/{verbId}/stage/{stage}")]
        public async Task<IActionResult> GetSelectedExamples(int lang, string verbId, int stage)
        {
            // if (stage < 3)
            // {
            //     stage = 3; // Copying an entry in Stage 1 needs to get Japanese examples which are stored in Stage 3. 
            // }

            try
            {
                var dddDb = _client.GetDatabase("ddd");
                var exampleCollection = dddDb.GetCollection<Example>("examples");
                var examples = await exampleCollection.FindAsync(
                    x => (
                        x.Status == STATUS.ACTIVE &&
                        x.Language == (LANG)lang &&
                        x.VerbId == verbId &&
                        x.Selected &&
                        (int)x.Stage == stage
                        ), new FindOptions<Example> { Sort = Builders<Example>.Sort.Ascending("LocalId") });

                return Ok(examples.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/comments/{linkId}")]
        public async Task<IActionResult> GetComments(string linkId)
        {
            try
            {
                var dddDb = _client.GetDatabase("ddd");
                var collection = dddDb.GetCollection<Comment>("comments");
                var comments = await collection.FindAsync(
                    x => x.Status == STATUS.ACTIVE &&
                    !string.IsNullOrEmpty(x.Value) &&
                    x.LinkId == linkId,
                    new FindOptions<Comment> { Sort = Builders<Comment>.Sort.Descending("Created") });

                return Ok(comments.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        private async Task<IList<T>> GetAllItems<T>(string verbId, int stage, bool onlySelected = true) where T : TextOption
        {
            var db = _client.GetDatabase("ddd");
            var collection = GetCollection<T>(db);
            var result = await collection.FindAsync(
                x => (
                    x.Status == STATUS.ACTIVE &&
                    !string.IsNullOrEmpty(x.Value) &&
                    x.VerbId == verbId &&
                    (!onlySelected || x.Selected) &&
                    (int)x.Stage <= stage
                    ),
                new FindOptions<T>
                { Sort = Builders<T>.Sort.Descending("Stage") });
        
            return result.ToList();
        }

        private async Task<IList<FullSentenceDefinition>> GetAllFullSentenceDefinitions(string verbId, int stage, bool onlySelected = true)
        {
            var dddDb = _client.GetDatabase("ddd");
            var fsdCollection = dddDb.GetCollection<FullSentenceDefinition>("full-sentence-definitions");
            var fsds = await fsdCollection.FindAsync(
                x => (
                    x.Status == STATUS.ACTIVE &&
                    !string.IsNullOrEmpty(x.Value) &&
                    x.VerbId == verbId &&
                    (onlySelected || x.Selected) &&
                    (int)x.Stage <= stage
                    ),
                new FindOptions<FullSentenceDefinition>
                { Sort = Builders<FullSentenceDefinition>.Sort.Descending("Stage") });
        
            return fsds.ToList();
        }

        private async Task<IList<FullSentenceDefinition>> DoListFullSentenceDefinitions(string verbId, int stage)
        {
            var dddDb = _client.GetDatabase("ddd");
            var fsdCollection = dddDb.GetCollection<FullSentenceDefinition>("full-sentence-definitions");
            var fsds = await fsdCollection.FindAsync(
                x => (
                    x.Status == STATUS.ACTIVE &&
                    !string.IsNullOrEmpty(x.Value) &&
                    x.VerbId == verbId &&
                    (int)x.Stage == stage
                    ),
                new FindOptions<FullSentenceDefinition>
                { Sort = Builders<FullSentenceDefinition>.Sort.Descending("Stage") });

            return fsds.ToList();
        }

        [HttpGet]
        [Route("api/fsds/verbId/{verbId}/stage/{stage}")]
        public async Task<IActionResult> GetFullSentenceDefinitions(string verbId, int stage)
        {
            try
            {
                return Ok(await GetAllFullSentenceDefinitions(verbId, stage, false /* with unselected ones*/));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/fsds/verbId/{verbId}/exactstage/{stage}")]
        public async Task<IActionResult> ListFullSentenceDefinitions(string verbId, int stage)
        {
            try
            {
                return Ok(await DoListFullSentenceDefinitions(verbId, stage));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/refBookNotes/verbId/{verbId}/stage/{stage}")]
        public async Task<IActionResult> GetReferenceBookNotes(string verbId, int stage)
        {
            return await GetCandidates<NoteForReferenceBook>(verbId, stage);
        }

        public async Task<IActionResult> GetCandidates<T>(string verbId, int stage) where T : TextOption
        {
            try
            {
                var db = _client.GetDatabase("ddd");
                var collection = GetCollection<T>(db);
                var findResults = await collection.FindAsync(
                    x => (
                        x.Status == STATUS.ACTIVE &&
                        !string.IsNullOrEmpty(x.Value) &&
                        x.VerbId == verbId &&
                        (int)x.Stage <= stage
                    ),
                    new FindOptions<T>
                    { Sort = Builders<T>.Sort.Descending("Stage") });
            
                var allCandidates = findResults.ToList();

                List<T> distinctCandidates = allCandidates
                    .GroupBy(f => f.Value)
                    .Select(g => g.First())
                    .ToList();

                var candidates = new List<T>();
                if (distinctCandidates != null)
                {
                    candidates.AddRange(distinctCandidates);
                }
 
                return Ok(candidates);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        // Shown in Settings tab
        [HttpGet]
        [Route("api/fsd/verbId/{verbId}/stage/{stage}")]
        public async Task<IActionResult> GetLatestFullSentenceDefinition(string verbId, int stage)
        {
            try
            {
                var allFsds = await GetAllFullSentenceDefinitions(verbId, stage);
                var latest = allFsds != null &&
                    allFsds.Count > 0 ?
                        allFsds[0] :
                        new FullSentenceDefinition
                        {
                            VerbId = verbId,
                            Stage = (STAGE)stage
                        };

                return Ok(latest);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/finalFsd/verbId/{verbId}")]
        public async Task<IActionResult> GetFinalFullSentenceDefinition(string verbId)
        {
            try
            {
                var allFsds = await GetAllFullSentenceDefinitions(verbId, (int)STAGE.STAGE_9); // all fsds under stage 9
                var final = allFsds != null && allFsds.Count > 0 ? allFsds[0] : new FullSentenceDefinition();

                return Ok(final);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        // For search
        [HttpGet]
        [Route("api/verbs/{value}/id/{id}")]
        public async Task<IActionResult> GetVerbs(string value, string id)
        {
            try
            {
                var dddDb = _client.GetDatabase("ddd");
                var verbCollection = dddDb.GetCollection<Verb>("verbs");
                var verbListResponse = new VerbListResponse();
                var verbs = await verbCollection.FindAsync
                    (
                        x => x.Id == id && x.Value.StartsWith(value),
                        new FindOptions<Verb> { Sort = Builders<Verb>.Sort.Ascending("Value") }
                    );

                var result = from v in verbs.ToList() select new { v.Id, v.Value, v.Structure };

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/verbs")]
        public async Task<IActionResult> GetVerbForLink([FromQuery]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Ok(null);
            }

            try
            {
                var db = _client.GetDatabase("ddd");
                var collection = db.GetCollection<Verb>("verbs");
                var verbs = await collection.FindAsync
                (
                    x => x.Id == id,
                    new FindOptions<Verb> { Sort = Builders<Verb>.Sort.Ascending("Value") }
                );
                var result = from v in verbs.ToList() select new { v.Id, v.Value, v.Structure, v.RepresentativeExample };
                return Ok(result.FirstOrDefault());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/verbs/{value}")]
        public async Task<IActionResult> GetVerbs(string value = "")
        {
            try
            {
                var db = _client.GetDatabase("ddd");
                var collection = db.GetCollection<Verb>("verbs");
                var verbs = await collection.FindAsync
                (
                    x => value == string.Empty || x.Value.StartsWith(value),
                    new FindOptions<Verb> { Sort = Builders<Verb>.Sort.Ascending("Value") }
                );

                return Ok(verbs.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/users")]
        public async Task<IActionResult> GetAllUsers()
        {
            var token = Request.Query["token"];
            try
            {
                var members = await _userService.GetAllUsersAsync(token);

                return Ok(members);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Fobidden")
                {
                    return StatusCode((int)HttpStatusCode.Forbidden, ex);
                }
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/jpmembers")]
        public async Task<IActionResult> GetJpMembers()
        {
            return await GetUsersByRoleAsync("jp-author");
        }

        [HttpGet]
        [Route("api/jpmanagers")]
        public async Task<IActionResult> GetJpManagers()
        {
            return await GetUsersByRoleAsync("jp-manager");
        }

        [HttpGet]
        [Route("api/enmembers")]
        public async Task<IActionResult> GetEnMembers()
        {
            return await GetUsersByRoleAsync("en-author");
        }

        [HttpGet]
        [Route("api/enmanagers")]
        public async Task<IActionResult> GetEnManagers()
        {
            return await GetUsersByRoleAsync("en-manager");
        }

        [HttpGet]
        [Route("api/managers")]
        public async Task<IActionResult> GetManagers() // 'jpManager' + 'enManager' + 'manager'
        {
            return await GetUsersByRoleAsync("manager");
        }

        [HttpGet]
        [Route("api/stageGroups")]
        public async Task<IActionResult> GetStageGroups()
        {
            return await GetUsersByRoleAsync("stage-group");
        }

        private async Task<IActionResult> GetUsersByRoleAsync(string role)
        {
            var token = Request.Query["token"];
            try
            {
                var users = await _userService.GetUsersByRoleAsync(role, token);

                return Ok(users);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Fobidden")
                {
                    return StatusCode((int)HttpStatusCode.Forbidden, ex);
                }
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/addVerb")]
        public async Task<IActionResult> AddVerb([FromBody] Verb verb)
        {
            verb.Id = Guid.NewGuid().ToString();
            verb.Created = verb.Updated = GetUtcEpoch();
            verb.Stage = STAGE.STAGE_1;
            verb.Status = STATUS.ACTIVE;

            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Verb>("verbs");
            await collection.InsertOneAsync(verb);
            var sortedVerbCursor = await collection.FindAsync
                (x => x.Status == STATUS.ACTIVE,
                new FindOptions<Verb>
                {
                    Sort = Builders<Verb>.Sort.Ascending("Value").Ascending("LocalIndex")
                });
            var sortedVerbs = sortedVerbCursor.ToList();
            var index = sortedVerbs.FindIndex(0, sortedVerbs.Count, x => x.Id == verb.Id);
            var page = index / NumItemsPerPage;

            return Ok(page);
        }

        [HttpPost]
        [Route("api/updateVerb")]
        public async Task<IActionResult> UpdateVerb([FromBody] Verb verb, [FromQuery] int? previousStage)
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Verb>("verbs");

            var verbsToUpdate = await collection.FindAsync(x => x.Id == verb.Id);
            var verbToUpdate = verbsToUpdate.ToList().FirstOrDefault();
            if (verbToUpdate == null)
            {
                return Ok(null);
            }

            var updatedVerb = await collection.FindOneAndReplaceAsync<Verb>(
                Builders<Verb>.Filter.Eq("Id", verb.Id),
                UpdateMessages(verb),
                new FindOneAndReplaceOptions<Verb>
                {
                    ReturnDocument = ReturnDocument.After
                }
            );

            if (previousStage != null) { // changing stage
                if ((int)verb.Stage > previousStage) { // upgrade, copy examples
                    if (previousStage >= 3) {
                        await CopyExamples(verb.Id, verb.Stage, (STAGE)previousStage, LANG.JP);
                    }
                    if (previousStage >= 4) {
                        await CopyExamples(verb.Id, verb.Stage, (STAGE)previousStage, LANG.EN);
                        await CopyFullSentenceDefinition(verb.Id, verb.Stage, (STAGE)previousStage);
                    }
                } else {
                    await CopyExamplesDownToStage1(verb, (STAGE)previousStage, LANG.JP);
                    await CopyExamplesDownToStage1(verb, (STAGE)previousStage, LANG.EN);
                    await DeleteCandidates(verb, LANG.JP);
                    await DeleteCandidates(verb, LANG.EN);
                    await CopyFullSentenceDefinition(verb.Id, verb.Stage, (STAGE)previousStage);
                }
            }
            return Ok(updatedVerb);
        }

        private async Task CopyExamplesDownToStage1(Verb verb, STAGE previousStage, LANG lang) {
            var nextStage = verb.Stage;
            for (var i = previousStage - 1; i >= nextStage; i--) {
                await CopyExamples(verb.Id, i, previousStage, lang);
            }
        }

        private async Task DeleteCandidates(Verb verb, LANG lang) {
            var db = _client.GetDatabase("ddd");
            var exampleColl = GetCollection<Example>(db);
            await exampleColl.DeleteManyAsync(x =>
                x.VerbId == verb.Id
                && x.Language == lang
                && (x.Stage == STAGE.STAGE_3 || x.Stage == STAGE.STAGE_7)
                && !x.Selected);
            var fsdColl = GetCollection<FullSentenceDefinition>(db);
            await fsdColl.DeleteManyAsync(x =>
                x.VerbId == verb.Id
                && !x.Selected);
        }

        private async Task CopyExamples(string verbId, STAGE nextStage, STAGE previousStage, LANG lang) {
            var db = _client.GetDatabase("ddd");
            var coll = GetCollection<Example>(db);
            for (int localId = 0; localId < 4; localId++) {
                var cursor = await coll.FindAsync<Example>(
                    x => (
                        x.Status == STATUS.ACTIVE &&
                        x.Selected &&
                        x.Language == (LANG)lang &&
                        x.VerbId == verbId &&
                        x.LocalId == localId &&
                        x.Stage == previousStage
                    )
                );
                var example = cursor.FirstOrDefault();
                var copy = new Example
                {
                    Id = Guid.NewGuid().ToString(),
                    VerbId = verbId,
                    Value = example != null && !string.IsNullOrEmpty(example.Value) ? example.Value : string.Empty,
                    Created = GetUtcEpoch(),
                    Stage = nextStage,
                    LocalId = localId,
                    Language = lang,
                    Selected = true,
                    Status = STATUS.ACTIVE
                };
                await coll.DeleteOneAsync<Example>(e =>
                    e.VerbId == verbId
                    && e.Status == STATUS.ACTIVE
                    && e.Language == (LANG)lang
                    && e.Stage == nextStage
                    && e.Selected
                    && e.LocalId == localId
                );
                await coll.InsertOneAsync(copy);
            }
        }

        private async Task CopyFullSentenceDefinition(string verbId, STAGE nextStage, STAGE previousStage) {
            var db = _client.GetDatabase("ddd");
            var coll = GetCollection<FullSentenceDefinition>(db);
            var cursor = await coll.FindAsync<FullSentenceDefinition>(
                x => (
                    x.Status == STATUS.ACTIVE &&
                    x.Selected &&
                    x.VerbId == verbId &&
                    x.Stage == previousStage
                )
            );
            var fsd = cursor.FirstOrDefault();
            if (fsd == null || string.IsNullOrEmpty(fsd.Value)) {
                return;
            }
            var copy = new FullSentenceDefinition
            {
                Id = Guid.NewGuid().ToString(),
                VerbId = fsd.VerbId,
                Value = fsd.Value,
                Created = GetUtcEpoch(),
                Stage = nextStage,
                Selected = true,
                Status = STATUS.ACTIVE
            };
            await coll.DeleteOneAsync<FullSentenceDefinition>(e =>
                e.VerbId == verbId
                && e.Status == STATUS.ACTIVE
                && e.Stage == nextStage
                && e.Selected
            );
            await coll.InsertOneAsync(copy);
        }

        private Verb UpdateMessages(Verb verb)
        {
            if (verb.Messages == null)
            {
                return verb;
            }

            foreach (var message in verb.Messages)
            {
                if (message.Created == null) // New message
                {
                    message.Id = Guid.NewGuid().ToString();
                    message.Created = GetUtcEpoch();
                    AddMessage(message);
                }
            }

            return verb;
        }

        private void AddMessage(Message message)
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Message>("messages");
            collection.InsertOne(message);
        }

        [HttpPost]
        [Route("api/cloneVerb")]
        public async Task<IActionResult> CloneVerb([FromBody] Verb verb)
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Verb>("verbs");
            verb.Id = Guid.NewGuid().ToString();
            await collection.InsertOneAsync(verb);

            return Ok(verb);
        }

        [HttpPost]
        [Route("api/updateExample")]
        public async Task<IActionResult> UpdateExample([FromBody] Example ex)
        {
            await Save(ex);

            return Ok(ex);
        }

        [HttpPost]
        [Route("api/updateExamples")]
        public async Task UpdateExamples([FromBody] IEnumerable<Example> examples)
        {
            await Update(examples);
        }

        [HttpPost]
        [Route("api/updateComment")]
        public async Task<IActionResult> UpdateComment([FromBody] Comment comment)
        {
            await Save(comment);

            return Ok(comment);
        }

        [HttpPost]
        [Route("api/updateComments")]
        public async Task UpdateComments([FromBody] IEnumerable<Comment> comments)
        {
            await Update(comments);
        }

        [HttpPost]
        [Route("api/updateFullSentenceDefinition")]
        public async Task<IActionResult> UpdateFullSentenceDefinition([FromBody] FullSentenceDefinition fsd)
        {
            await Save(fsd);

            return Ok(fsd);
        }

        [HttpPost]
        [Route("api/updateFullSentenceDefinitions")]
        public async Task UpdateFullSentenceDefinitions([FromBody] IEnumerable<FullSentenceDefinition> fsds)
        {
            foreach (var fsd in fsds)
            {
                await Save(fsd);
            }
        }

        [HttpPost]
        [Route("api/updateReferenceBookNote")]
        public async Task<IActionResult> UpdateReferenceBookNote([FromBody] NoteForReferenceBook note)
        {
            await Save(note);

            return Ok(note);
        }

        [HttpPost]
        [Route("api/updateReferenceBookNotes")]
        public async Task UpdateReferenceBookNotes([FromBody] IEnumerable<NoteForReferenceBook> notes)
        {
            foreach (var note in notes)
            {
                await Save(note);
            }
        }

        public async Task Update<T>([FromBody] IEnumerable<T> collection) where T : Text // updates a collection of T
        {
            foreach (var item in collection)
            {
                await Save(item);
            }
        }
        private async Task Save<T>(T item) where T : Text
        {
            var db = _client.GetDatabase("ddd");
            var collection = GetCollection<T>(db);
            var itemsToUpdate = await collection.FindAsync
            (
                x => x.Id == item.Id
            );
            var itemToUpdate = itemsToUpdate.ToList().FirstOrDefault();
            if (itemToUpdate == null || item.Stage > itemToUpdate.Stage) // Brand new or the text has been modified at a different stage.
            {
                item.Id = Guid.NewGuid().ToString();
                item.Created = item.Updated = GetUtcEpoch();
                await collection.InsertOneAsync(item);
            }
            else
            {
                item.Updated = GetUtcEpoch();
                var updated = await collection.FindOneAndReplaceAsync<T>(
                    x => x.Id == item.Id && x.Stage == item.Stage, //Builders<T>.Filter.Eq("Id", item.Id),
                    item,
                    new FindOneAndReplaceOptions<T>
                    {
                        ReturnDocument = ReturnDocument.After
                    }
                );
            }
        }

        private IMongoCollection<T> GetCollection<T>(IMongoDatabase db) where T : class
        {
            if (db == null)
            {
                throw new ArgumentException("db must not be null");
            }

            if (typeof(T) == typeof(Example))
            {
                return db.GetCollection<T>("examples");
            }

            if (typeof(T) == typeof(Comment))
            {
                return db.GetCollection<T>("comments");
            }

            if (typeof(T) == typeof(FullSentenceDefinition))
            {
                return db.GetCollection<T>("full-sentence-definitions");
            }

            if (typeof(T) == typeof(NoteForReferenceBook))
            {
                return db.GetCollection<T>("note-for-reference-book");
            }

            return null;
        }

        [HttpGet]
        [Route("api/suggestions/verbId/{verbId}")]
        [ProducesResponseType(200, Type = typeof(Suggestion))]
        public async Task<IActionResult> GetSuggestions(string verbId)
        {
            try
            {
                var db = _client.GetDatabase("ddd");
                var collection = db.GetCollection<Suggestion>("suggestions");
                var items = await collection.FindAsync
                (
                    x => x.VerbId == verbId
                );
                var suggestion = items.ToList().FirstOrDefault();
                return Ok(suggestion);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/updateSuggestions/verbId/{verbId}")]
        [ProducesResponseType(200, Type = typeof(Suggestion))]
        public async Task<IActionResult> UpdateSuggestions([FromBody]Suggestion value, string verbId)
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<Suggestion>("suggestions");

            value.VerbId = verbId;

            var itemsToUpdate = await collection.FindAsync(x => x.VerbId == verbId);
            var itemToUpdate = itemsToUpdate.ToList().FirstOrDefault();
            if (itemToUpdate == null)
            {
                value.Id = Guid.NewGuid().ToString();
                collection.InsertOne(value);
                return Ok(value);
            }

            var updated = await collection.FindOneAndReplaceAsync<Suggestion>(
                Builders<Suggestion>.Filter.Eq("VerbId", verbId),
                value,
                new FindOneAndReplaceOptions<Suggestion>
                {
                    ReturnDocument = ReturnDocument.After
                }
            );

            return Ok(updated);
        }

        [HttpGet]
        [Route("api/finalCheckList/verbId/{verbId}")]
        [ProducesResponseType(200, Type = typeof(FinalCheckList))]
        public async Task<IActionResult> GetFinalCheckList(string verbId)
        {
            try
            {
                var db = _client.GetDatabase("ddd");
                var collection = db.GetCollection<FinalCheckList>("final-check-list");
                var items = await collection.FindAsync
                (
                    x => x.VerbId == verbId
                );
                var list = items.ToList().FirstOrDefault();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/finalCheckList/verbId/{verbId}")]
        [ProducesResponseType(200, Type = typeof(FinalCheckList))]
        public async Task<IActionResult> UpdateFinalCheckList([FromBody]FinalCheckList value, string verbId)
        {
            var db = _client.GetDatabase("ddd");
            var collection = db.GetCollection<FinalCheckList>("final-check-list");

            if (!value.EnglishIndex && !value.Kanji && !value.Link1 && !value.Link2) // Remove check list
            {
                await collection.DeleteOneAsync<FinalCheckList>(x => x.VerbId == verbId);
                return Ok();
            }

            value.VerbId = verbId;

            var itemsToUpdate = await collection.FindAsync(x => x.VerbId == verbId);
            var itemToUpdate = itemsToUpdate.ToList().FirstOrDefault();
            if (itemToUpdate == null)
            {
                value.Id = Guid.NewGuid().ToString();
                collection.InsertOne(value);
                return Ok(value);
            }

            var updated = await collection.FindOneAndReplaceAsync<FinalCheckList>(
                Builders<FinalCheckList>.Filter.Eq("VerbId", verbId),
                value,
                new FindOneAndReplaceOptions<FinalCheckList>
                {
                    ReturnDocument = ReturnDocument.After
                }
            );

            return Ok(updated);
        }

        private static long GetUtcEpoch()
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);

            return (long)t.TotalMilliseconds;
        }
    }
}
