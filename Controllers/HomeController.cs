﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace d3_author.Controllers {
  public class HomeController : Controller {
    public HomeController() {}

    public IActionResult Index()
    {
      return View ();
    }
  }
}