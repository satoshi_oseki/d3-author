using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using d3_author.Models;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using d3_author.Models.Optons;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using MongoDB.Bson;

namespace d3_author.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private MongoClient _client;
        private List<User> cache;

        public UserService(IOptions<AppSettings> optionsAccessor)
        {
            _appSettings = optionsAccessor.Value;
            var mongoDbConnection = _appSettings.MongoDbConnection;//configuration.GetSection("AppSettings").GetValue<string>("MongoDbConnection");
            _client = new MongoClient(mongoDbConnection);
        }

        private async Task CacheUsers(string token)
        {
            var db = _client.GetDatabase("ddd-admin");
            var collection = db.GetCollection<User>("users");
            var cursor = await collection.Find<User>(new BsonDocument())
                .Project<User>
                (
                    Builders<User>.Projection
                        .Include("userId")
                        .Include("username")
                        .Include("displayName")
                        .Include("roles")
                ).ToListAsync();
            cache = cursor;
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync(string token)
        {
            if (cache == null)
            {
                await CacheUsers(token);
            }

            return cache;
        }

        public async Task<User> GetUser(string username, string token)
        {
            if (cache == null)
            {
                await CacheUsers(token);
            }

            var user = cache.Find(x => x.Username == username);

            return user;
        }

        public async Task<User> GetUserById(string userId, string token)
        {
            if (cache == null)
            {
                await CacheUsers(token);
            }

            var user = cache.Find(x => x.UserId == userId);

            return user;

        }

        public async Task<IEnumerable<User>> GetUsersByRoleAsync(string role, string token)
        {
            var db = _client.GetDatabase("ddd-admin");
            var filter = Builders<User>.Filter.Regex("roles", new BsonRegularExpression($"{role}", "i"));
            var collection = db.GetCollection<User>("users");
            return await collection.Find(filter).Project<User>
                (
                    Builders<User>.Projection
                        .Include("userId")
                        .Include("username")
                        .Include("displayName")
                        .Include("roles")
                ).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsersByRoles(string[] roles, string token)
        {
            List<User> userCollection = new List<User>();

            foreach (var role in roles)
            {
                var users = await GetUsersByRoleAsync(role, token);
                userCollection.AddRange(users);
            }

            return userCollection;
        }

        public string GetDefaultUserId(STAGE sTAGE) // for import return uuid
        {
            switch(sTAGE)
            {
                case STAGE.STAGE_1: return GetUserFromCache("stage1group").UserId;
                case STAGE.STAGE_4: return GetUserFromCache("hayashishita").UserId;
                case STAGE.STAGE_5: return GetUserFromCache("stage5group").UserId;
                case STAGE.STAGE_8: return GetUserFromCache("payne").UserId;
                case STAGE.STAGE_9: return GetUserFromCache("tanaka").UserId;
                default: return "";
            }
        }

        private User GetUserFromCache(string username)
        {
            var user = cache.Find(x => x.Username == username);

            return user;
        }

    }
}
