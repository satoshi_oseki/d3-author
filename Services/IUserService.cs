using System.Collections.Generic;
using System.Threading.Tasks;
using d3_author.Models;

namespace d3_author.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetUsersByRoleAsync(string role, string token);
        Task<IEnumerable<User>> GetUsersByRoles(string[] role, string token);
        Task<IEnumerable<User>> GetAllUsersAsync(string token);
        Task<User> GetUser(string username, string token);
        Task<User> GetUserById(string userId, string token);
        string GetDefaultUserId(STAGE sTAGE);
    }
}
